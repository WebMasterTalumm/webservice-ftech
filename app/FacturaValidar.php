<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaValidar extends Model
{
    protected $table = "facturas_validar";    

    public function detalles(){
        return $this->hasMany('App\DetalleFactValidar', 'id_factura');
    }
    public function municipios(){
        return $this->belongsTo('App\Municipio', 'id_municipio');
    }
}
