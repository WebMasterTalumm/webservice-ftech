<?php

namespace App\params;

use App\Matricula;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use  App\Ajuste;



class Params
{

    public function getparams () {

        $ajustes=Ajuste::first();
        $wsdlurl =  $ajustes->wsdllrl;
        $username =  $ajustes->username;
        $password = $ajustes->password;
        // dd($ajustes);
        // return [
        //     "username" => 'erp_PagoPass',
        //     "password" => 'b8e888d15c1e62036b81dfbe62300dd3fd06405e411503cc29a2105a2eddc25e',
        //     "wsdlUrl" => 'https://ws.facturatech.co/21/index.php?wsdl'
        // ];
        return [
            "username" => $username,
            "password" => $password,
            "wsdlUrl" =>$wsdlurl
        ];
      }


}
