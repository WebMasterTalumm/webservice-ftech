<?php

namespace App\params;

use App\Factura;
use App\Nota;
use App\DetalleFactura;
use App\DetalleNota;
use App\LogImportacion;
use App\EmisorFactura;
use App\Responsabilidad;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\DB;
use File;
use XMLWriter;

class XmlNC
{


    public function Xml(){
		$request = request();
		// dd($request->numero);
        $notas = Nota::where('numero','=',$request->numero)
        ->with('municipios')
		->with('detalles')
		->with('facturas')
		->get();
		dd($notas);
       $detalles1= DetalleNota::get();
    //    dd($detalles1->facturas);
        $emisores = EmisorFactura::get();
        $resultado = array();
        $ref=array();
        $normas=[];
		$ident_tipo_doc="ND";
		$Vers_esqu_UBL="UBL 2.1";
		$Versión_for_doc="DIAN 2.1";
		foreach ($notas as $nota) {
			foreach ($emisores as $emisor) {
				$id_factura=$nota['id_factura'];
				$factura= Factura::select('facturas.*')->where('id','=',$id_factura)->first();
				$cufe=$nota->facturas->cufe;
				$ident_cufe='CUFE-SHA384';
				$fecha=$nota->fecha;
				$fecha_ven=$nota->fecha_vencimiento;
				// dd($fecha_ven);
						// $fecha_y_hora=explode(" ",  $fecha_hora);
					//convertir fecha a formato indicado
						// $fecha=strtotime($fecha_y_hora[ 0]);
						$fec= explode("-", $fecha);
						$mes1=$fec[1];
						$fecve=explode("-", $fecha_ven);
						$mes2=$fecve[1];
							switch ($mes1) {
								case 'ENE':
									$mes='01';
									break;
								case 'FEB':
									$mes='02';
									break;
								case 'MAR':
									$mes='03';
									break;
								case 'ABR':
									$mes='04';
									break;
								case 'MAY':
									$mes='05';
									break;
								case 'JUN':
									$mes='06';
									break;
								case 'JUL':
									$mes='07';
									break;
								case 'AGO':
									$mes='08';
									break;
								case 'SEP':
									$mes='09';
									break;
								case 'OCT':
									$mes='10';
									break;
								case 'NOV':
									$mes='11';
									break;
								case 'DIC':
									$mes='12';
									break;
							};
							switch ($mes2) {
								case 'ENE':
									$mesv='01';
									break;
								case 'FEB':
									$mesv='02';
									break;
								case 'MAR':
									$mesv='03';
									break;
								case 'ABR':
									$mesv='04';
									break;
								case 'MAY':
									$mesv='05';
									break;
								case 'JUN':
									$mesv='06';
									break;
								case 'JUL':
									$mesv='07';
									break;
								case 'AGO':
									$mesv='08';
									break;
								case 'SEP':
									$mesv='09';
									break;
								case 'OCT':
									$mesv='10';
									break;
								case 'NOV':
									$mesv='11';
									break;
								case 'DIC':
									$mesv='12';
									break;
                            };

					//convertir hora a formato indicado
						// $time_12=$fecha_y_hora[1].' PM';

						// $hora1= new DateTime($time_12);
						// $time_24 = $hora1->format('H:i:s');
				$fec_emision=$fec[0]."-".$mes."-".$fec[2];
				$nit_emisor=$emisor["nit_emisor"];
				$fecha_vencimiento=$fecve[0]."-".$mesv."-".$fecve[2];
				// dd($fecha_vencimiento);
						$nit_emisore=explode("-",$nit_emisor);
						$nit=$nota->identifi_adqui;
						$tip_nota=$nota['tipo_documento'];

					$tot_detalle=$nota['detalles'];

					// dd($tot_detalle);
					$total_lineas="";
					$total_lineas=count($tot_detalle);
					// dd($total_lineas);
					$indicador_tip_opera="10";
					$Código_descr_ambiente="2";//2=prueba  1=produccion
					$div_conso=$emisor["divisa_consolidada"];
					//start variables  emisor
					$div_conso=$emisor["divisa_consolidada"];
					$tipo_ident_emisor=$emisor["tipo_contribuyente"];
					$tipo_doc_ident_emisor=$emisor["tip_documento"];  //Tipos de documentos de identidad
					$regimen_emisor=$emisor["regimen"];
					$nombre_emisor=$emisor["razon_social"];
					$direccion_emisor=$emisor["direccion"];
					$cod_depar_emisor=$emisor->municipios->codigo_departamento;
					// dd($cod_depar_emisor);
					$ciudad_emisor=$emisor->municipios->nombre_municipio;
					$cod_pais_emisor=$emisor['cod_pais'];
					$nombre_depar_emisor=$emisor->municipios->nombre_departamento;
					$codigo_muni_emisor=$emisor->municipios->codigo_departamento;
					$codigo_ciuu_emisor=$emisor['cod_ciuu'];
						$tip_cont_emisor=$emisor['tipo_contacto'];
						$contact_emisor=$emisor['nombre_contacto'];
						$num_contact_emisor=$emisor['telefono_contacto'];
						$email_cont_emisor=$emisor['email_contacto'];
						$ident_tributo=$emisor['identifi_tributo'];
						$nom_tributo=$emisor['nombre_tributo'];
						//End variables emisor
						$identi_tip_persona=$nota['ident_tip_pers'];
						// dd($identi_tip_persona);
						$tdifp=$nota['tip_identifi'];
						$regimen_adq=$nota['regimen'];
						$cod_depar_adq=@$nota->municipios->codigo_departamento;
						$cliente=$nota->razon_social;
						$nombre_adqui=@$nota->nombre_adqui;
						$apellido_adqui=@$nota->apellidos_adqui;
						$nit=$nota->identifi_adqui;
						$nit_adqui=explode("-",$nit);
						$nit_adqui_sidig=$nit_adqui[0];
						$digito_verificacion=$nit_adqui[1];
						$nit_adquiriente= $nit_adqui_sidig;
						$direc_cliente=$nota->direccion;
						$cod_pais_adq='CO';
						$nombre_depar_adq=@$nota->municipios->nombre_departamento;
						$ciudad_adq=@$nota->municipios->nombre_municipio;
						$codigo_muni_adq=@$nota->municipios->codigo_municipio;
						$cod_pais='CO';
						$nom_pais="COLOMBIA";
						$contact_adq=$nota['nombre_contacto'];
						$num_contact_adq=$nota['telefono'];
						$email_cont_adq=$nota['email_contacto'];
						$telefono_cont_adq=$nota['telefono_contacto'];

						$detalles= $factura->detalles;
				// dd($detalles);
				$base_imponible=0;
				$total_bruto=0;
				$valor_total_impuesto=0;
				$descuentos=0;
				$tarifa_tributo=0;
				$tota=0;
				//retenciones
				//retenciones
				$rte_iv=intval($nota->rte_iva);

				$rte_ic=intval($nota->rte_ica);

				// $rte_cre=strtolower($factura->rte_cree);
                $rte_fuent=intval($nota->rte_fuente);
				// dd($valor_retenciones);
				// $valor_retenciones=0;
				$elemento="false";

	$objetoXML = new XMLWriter();
	$objetoXML->openMemory();
	$objetoXML->startDocument('1.0', 'utf-8');
	$objetoXML->setIndent(true);
	$objetoXML->startElement('NOTA');
	$objetoXML->writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	$objetoXML->writeAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
	// $objetoXML->endElement();// Final del elemento
			$objetoXML->startElement("ENC");
				$objetoXML->startElement("ENC_1");
				$objetoXML->text($ident_tipo_doc); //Identificador del tipo de documento.Se debe enviar según corresponda el tipo de documento:
				$objetoXML->endElement();// Final del elemento
				$objetoXML->startElement("ENC_2");
				$objetoXML->text($nit_emisore[0]);//Identificación del obligado a facturar electrónico - NIT.
				// $objetoXML->text("901143311");//Identificación del obligado a facturar electrónico - NIT.
				$objetoXML->endElement();// Final del elemento
				$objetoXML->startElement("ENC_3");
				$objetoXML->text($nit_adqui[0]);//Identificación del adquiriente - NIT.
				$objetoXML->endElement();// Final del elemento
				$objetoXML->startElement("ENC_4");
				$objetoXML->text($Vers_esqu_UBL);
				$objetoXML->endElement();// Final del elemento
				$objetoXML->startElement("ENC_5");
				$objetoXML->text($Versión_for_doc);
				$objetoXML->endElement();// Final del elemento
				$objetoXML->startElement("ENC_6");
				$objetoXML->text($nota['numero']);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("ENC_7");
				$objetoXML->text($fec_emision);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("ENC_8");
				$objetoXML->text('18:42:39');
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("ENC_9");
				$objetoXML->text('91');
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("ENC_10");
				$objetoXML->text($div_conso);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("ENC_15");
				$objetoXML->text($total_lineas);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("ENC_16");
				$objetoXML->text($fecha_vencimiento);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("ENC_20");
				$objetoXML->text($Código_descr_ambiente);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("ENC_21");
				$objetoXML->text($nota['tipo_operacion']);
				$objetoXML->endElement(); // Final del elemento
			$objetoXML->endElement();
			$objetoXML->startElement("EMI");
					$objetoXML->startElement("EMI_1");
					$objetoXML->text($tipo_ident_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_2");
					$objetoXML->text($nit_emisore[0]);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_3");
					$objetoXML->text($tipo_doc_ident_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_4");
					$objetoXML->text($regimen_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_6");
					$objetoXML->text($emisor['razon_social']);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_7");
					$objetoXML->text($emisor['nombre_comercial']);//NOMBRE COMERCIAL EMISOR
					$objetoXML->endElement(); // Final del elemento
					// $objetoXML->startElement("EMI_6");
					// $objetoXML->text($nombre_emisor);
					// $objetoXML->endElement(); // Final del elemento
					// $objetoXML->startElement("EMI_7");
					// $objetoXML->text($nombre_emisor);//NOMBRE COMERCIAL EMISOR
					// $objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_10");
					$objetoXML->text($direccion_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_11");
					$objetoXML->text($cod_depar_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_13");
					$objetoXML->text($ciudad_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_14");
					$objetoXML->text($cod_pais_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_15");
					$objetoXML->text($indicador_tip_opera);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_19");
					$objetoXML->text($nombre_depar_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_21");
					$objetoXML->text($nom_pais);
					$objetoXML->endElement(); // Final del elemento // Final del elemento
					$objetoXML->startElement("EMI_22");
					$objetoXML->text($nit_emisore[1]);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_23");
					$objetoXML->text($codigo_muni_emisor);
					// dd($codigo_muni_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_24");
					$objetoXML->text($regimen_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("EMI_25");
					$objetoXML->text($codigo_ciuu_emisor);//Corresponde al código de actividad económica CIIU.
                    $objetoXML->endElement(); // Final del elemento
                    $responsabilidades=Responsabilidad::select('cod')->get();
                    $size=sizeOf($responsabilidades);
                    $aTarea = array();

                        foreach ($responsabilidades as $responsa) {
                                $arrar = $responsa['cod'];
                                array_push($aTarea, $arrar);
                            }
                    $respon=implode(";", $aTarea);
				$objetoXML->startElement("TAC");
					$objetoXML->startElement("TAC_1");
					$objetoXML->text($respon);//Obligaciones del contribuyente
					$objetoXML->endElement(); // Final del elemento
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("DFE");
					$objetoXML->startElement("DFE_1");
					$objetoXML->text($codigo_muni_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("DFE_2");
					$objetoXML->text($cod_depar_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("DFE_3");
					$objetoXML->text($cod_pais_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("DFE_4");
					$objetoXML->text($nota->cod_postal);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("DFE_5");
					$objetoXML->text('');
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("DFE_6");
					$objetoXML->text($nombre_depar_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("DFE_7");
					$objetoXML->text($ciudad_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("DFE_8");
					$objetoXML->text($direccion_emisor);
					// dd($direccion_emisor);
					$objetoXML->endElement(); // Final del elemento
				$objetoXML->endElement(); // Final del elemento
                $objetoXML->startElement("ICC");
                $num_matri=$emisor->num_matri_mercan;
					$objetoXML->startElement("ICC_1");//Número de matrícula mercantil (Identificador de sucursal: Punto de facturación)
					$objetoXML->text($num_matri);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("ICC_2");
					$objetoXML->text("");
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("ICC_4");
					$objetoXML->text("");
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("ICC_5");
					$objetoXML->text("");
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("ICC_6");
					$objetoXML->text("");
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("ICC_8");
					$objetoXML->text("");
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("ICC_9");
					$objetoXML->text("");
					$objetoXML->endElement();
				$objetoXML->endElement();
				$objetoXML->startElement("CDE");
					$objetoXML->startElement("CDE_1");
					$objetoXML->text($tip_cont_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("CDE_2");
					$objetoXML->text($contact_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("CDE_3");
					$objetoXML->text($num_contact_emisor);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("CDE_4");
					$objetoXML->text($email_cont_emisor);
					$objetoXML->endElement(); // Final del elemento
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("GTE");
					$objetoXML->startElement("GTE_1");
					$objetoXML->text($ident_tributo);
					$objetoXML->endElement(); // Final del elemento
					$objetoXML->startElement("GTE_2");
					$objetoXML->text($nom_tributo);
					$objetoXML->endElement(); // Final del elemento
				$objetoXML->endElement();
			$objetoXML->endElement();
			$objetoXML->startElement("ADQ");
			$objetoXML->startElement("ADQ_1");
			$objetoXML->text($identi_tip_persona);
			// dd($identi_tip_persona);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_2");//Identificador del adquiriente
			$objetoXML->text($nit_adquiriente);
			// '13'= Cédula de ciudadanía
			// '21'= Tarjeta de extranjería
			// '22'= Cédula de extranjería
			// '31'= NIT
			// '41'= Pasaporte
			$objetoXML->endElement(); // Final del elemento
			//13	Cédula de ciudadanía
			//21	Tarjeta de extranjería
			//22	Cédula de extranjería
			//31	NIT
			$objetoXML->startElement("ADQ_3");
			$objetoXML->text($tdifp);//Tipo de documento de identificación fiscal de la persona
			$objetoXML->endElement(); // Final del elemento
				//48	Impuestos sobre la venta del IVA
				//49	No responsables del IVA
			$objetoXML->startElement("ADQ_4");
			$objetoXML->text($nota->regimen);//Régimen: Régimen al que pertenece.
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_6");
			$objetoXML->text($cliente);//Razón social de la empresa.  
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_8");//Nombre del adquiriente.
			$objetoXML->text($nombre_adqui);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_9"); //Apellidos del adquiriente Para informarse si ADQ_1="2" "Persona Natural".
			$objetoXML->text($apellido_adqui);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_10");//dirección adquiriente
			$objetoXML->text($direc_cliente);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_11");//Código del Departamento. adquiriente
			// dd($nombre_depar);
			$objetoXML->text($cod_depar_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_13"); //Nombre de la ciudad.
			$objetoXML->text($ciudad_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_14");//Código postal.
			$objetoXML->text($nota->cod_postal);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_15");//Código identificador del país.
			$objetoXML->text($cod_pais_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_19");//Nombre del Departamento
			$objetoXML->text($nombre_depar_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_21");//Nombre del País
			$objetoXML->text($nom_pais);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_22");//Digito de verificación -DV del NIT del adquiriente
			$objetoXML->text($digito_verificacion);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ADQ_23");//Código del municipio.
			$objetoXML->text($codigo_muni_adq);
			$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TCR");
			$objetoXML->startElement("TCR_1");//Responsabilidades del adquiriente.
			$objetoXML->text($nota->responsa_adqui);
			$objetoXML->endElement();// Final del elemento
		$objetoXML->endElement();
		$objetoXML->startElement("ILA");
			$objetoXML->startElement("ILA_1");//Nombre registrado en el RUT.
			$objetoXML->text($nota['nom_regis_rut']);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ILA_2");//Identificador del adquiriente.
			$objetoXML->text($nit_adqui[0]);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ILA_3");//Tipo de documento de identificación fiscal de la persona
			$objetoXML->text($nota->tip_identifi);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("ILA_4");//Tipo de documento de identificación fiscal de la persona
			$objetoXML->text($nit_adqui[1]);
			$objetoXML->endElement(); // Final del elemento
		$objetoXML->endElement();
		$objetoXML->startElement("DFA");
			$objetoXML->startElement("DFA_1");//Código Identificador del país
			$objetoXML->text($cod_pais);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("DFA_2");//Código del Departamento
			$objetoXML->text($cod_depar_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("DFA_3");//Código postal
			$objetoXML->text($nota->cod_postal);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("DFA_4");//Código del municipio
			$objetoXML->text($codigo_muni_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("DFA_5");//Nombre del País
			$objetoXML->text($nom_pais);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("DFA_6");//Nombre del Departamento
			$objetoXML->text($nombre_depar_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("DFA_7");//Nombre de la ciudad
			$objetoXML->text($ciudad_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("DFA_8");//informaciones de su dirección
			$objetoXML->text($direc_cliente);
			$objetoXML->endElement(); // Final del elemento
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("CDA");
			$objetoXML->startElement("CDA_1");//Tipo de contacto, del adquiriente Persona de contacto
			$objetoXML->text($nota['tipo_contacto']);
			$objetoXML->endElement();
			$objetoXML->startElement("CDA_2");//Nombre y cargo de la persona de contacto
			// dd($contact_adq);
			$objetoXML->text($contact_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("CDA_3");//Teléfono de la persona de contacto
			$objetoXML->text($telefono_cont_adq);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("CDA_4");//Correo electrónico  de la persona de contacto.
			$objetoXML->text($email_cont_adq);
			$objetoXML->endElement(); // Final del elemento
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("GTA");
			$objetoXML->startElement("GTA_1");//Identificador del tributo
			$objetoXML->text($nota['ident_tributo']);
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("GTA_2");//Nombre del tributo
			$objetoXML->text($nota['nom_tributo']);
			$objetoXML->endElement(); // Final del elemento
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->endElement(); // Final del elemento	}
	$objetoXML->startElement("TOT");
	$val_descu=0;
	$val_iva=0;
	foreach ($nota['detalles'] as $detal) {
		$total_bruto+=$detal['cantidad']*$detal['precio_unit'];// TOT_1
		$base_imponible+=$detal->valor_total; // TOT_3
		$tarifa_tributo=$detal->impuesto;
		$descuentos+=$detal->descuento;
		$val_descu+=$detal['cantidad']*$detal['precio_unit']*$detal['descuento']/100;
		$valort=$detal['cantidad']*$detal['precio_unit'];
		$val_descitem=$valort*$detal['descuento']/100;
		$valorto=$valort-$val_descitem;
		$val_iva+=$valorto*$detal['impuesto']/100;
	}
	$tarifa_tribut=19;
	$porcentaje_descuento=$descuentos/100;
	$total5=$total_bruto-$val_descu;
    $total2=$total5+$val_iva;
    $moneda_importe="COP";
	$tot5=$total5+$val_iva;
	$toto=$total_bruto+$val_iva;
	$objetoXML->startElement("TOT_1");//Valor bruto antes de tributos
	$objetoXML->text($total5);
	$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("TOT_2");//Moneda total Importe bruto antes de impuestos
	$objetoXML->text($moneda_importe);
	$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("TOT_3");//El Valor Base Imponible tiene que ser la suma de los valores de las bases imponibles de todas líneas de detalle.
	$objetoXML->text($total5);
	$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("TOT_4");//Moneda del total Base Imponible
	$objetoXML->text($moneda_importe);
	$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("TOT_5");//Valor total de ítems (incluyendo cargos y descuentos a nivel de ítems)
	$objetoXML->text($tot5);
	$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("TOT_6");//Moneda del total de la factura.
	$objetoXML->text($moneda_importe);
	$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("TOT_7");//Total de Valor Bruto más tributos
	$objetoXML->text($total2);
	$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("TOT_8");//Moneda Total de Valor bruto con tributos
	$objetoXML->text($moneda_importe);
	$objetoXML->endElement(); // Final del elemento
$objetoXML->endElement(); // Final del elemento
$objetoXML->startElement("TIM");
		$objetoXML->startElement("TIM_1");//impuesto
		$objetoXML->text($elemento);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TIM_2");//Valor del tributo.
		$objetoXML->text($val_iva);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TIM_3");
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("IMP");
		// $objetoXML->startElement("IMP_1");
		// $objetoXML->text($ident_tributo);
		// $objetoXML->endElement(); // Final del elemento
		$identi_tributo=01;
		$objetoXML->startElement("IMP_1");
		$objetoXML->text($factura['ident_tributo']);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_2");
		$objetoXML->text($total5);//Base Imponible sobre la que se calcula el valor del tributo.
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_3");
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_4");//Valor del tributo
		$objetoXML->text($val_iva);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_5");//Moneda del valor del tributo
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_6");//Tarifa del tributo
		$objetoXML->text($tarifa_tributo);
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->endElement(); // Final del elemento
$objetoXML->endElement(); // Final del elemento
if ($rte_iv != 0 || $rte_iv != 0.0) {
	$elemento_rt_iva='true';
	$tari_trib_rtiva=15;
	$rte_iva=$val_iva*0.15;
	$objetoXML->startElement("TIM");
		$objetoXML->startElement("TIM_1");//impuesto
		$objetoXML->text($elemento_rt_iva);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TIM_2");//Valor del tributo.
		$objetoXML->text($rte_iva);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TIM_3");
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("IMP");
		// $objetoXML->startElement("IMP_1");
		// $objetoXML->text($ident_tributo);
		// $objetoXML->endElement(); // Final del elemento
		$iden_trib_rteiva='05';
		$objetoXML->startElement("IMP_1");
		$objetoXML->text($iden_trib_rteiva);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_2");
		$objetoXML->text($val_iva);//Base Imponible sobre la que se calcula el valor del tributo.
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_3");
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_4");//Valor del tributo
		$objetoXML->text($rte_iva);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_5");//Moneda del valor del tributo
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_6");//Tarifa del tributo
		$objetoXML->text($tari_trib_rtiva);
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->endElement(); // Final del elemento
$objetoXML->endElement(); // Final del elemento
}
if($rte_ic != 0 || $rte_ic != 0.0){
	$elemento_rt_ica='true';
	$iden_trib_rteica='07';
	$tari_trib_rtica=0.77;
	$rte_ica=$total5*0.77/100;
	$objetoXML->startElement("TIM");
		$objetoXML->startElement("TIM_1");//impuesto
		$objetoXML->text($elemento_rt_ica);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TIM_2");//Valor del tributo.
		$objetoXML->text($rte_ica);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TIM_3");
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("IMP");
		$objetoXML->startElement("IMP_1");
		$objetoXML->text($iden_trib_rteica);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_2");
		$objetoXML->text($total5);//Base Imponible sobre la que se calcula el valor del tributo.
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_3");
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_4");//Valor del tributo
		$objetoXML->text($rte_ica);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_5");//Moneda del valor del tributo
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_6");//Tarifa del tributo
		$objetoXML->text($tari_trib_rtica);
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->endElement(); // Final del elemento
$objetoXML->endElement(); // Final del elemento
}
if($rte_fuent != 0 || $rte_fuent != 0.0){
	$elemento_rte_fuente='true';
	$tari_trib_rtefuen=2.50;
	$rte_fuente=$total5*2.50/100;
	$objetoXML->startElement("TIM");
		$objetoXML->startElement("TIM_1");//impuesto
		$objetoXML->text($elemento_rte_fuente);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TIM_2");//Valor del tributo.
		$objetoXML->text($rte_fuente);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("TIM_3");
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->startElement("IMP");
		$iden_trib_rtefuen='06';
		$objetoXML->startElement("IMP_1");
		$objetoXML->text($iden_trib_rtefuen);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_2");
		$objetoXML->text($total5);//Base Imponible sobre la que se calcula el valor del tributo.
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_3");
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_4");//Valor del tributo
		$objetoXML->text($rte_fuente);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_5");//Moneda del valor del tributo
		$objetoXML->text($moneda_importe);
		$objetoXML->endElement(); // Final del elemento
		$objetoXML->startElement("IMP_6");//Tarifa del tributo
		$objetoXML->text($tari_trib_rtefuen);
		$objetoXML->endElement(); // Final del elemento
	$objetoXML->endElement(); // Final del elemento
$objetoXML->endElement(); // Final del elemento
}
            $numero_autorizacion=$nota['num_autorizacion'];
            $fecin_autorizacion=$nota['fec_ini_autorizacion'];
            $fecautini= explode("-", $fecin_autorizacion);
            $mesautini=$fecautini[0];
                switch ($mesautini) {
                    case 'ENE':
                        $mes0='01';
                        break;
                    case 'FEB':
                        $mes0='02';
                        break;
                    case 'MAR':
                        $mes0='03';
                        break;
                    case 'ABR':
                        $mes0='04';
                        break;
                    case 'MAY':
                        $mes0='05';
                        break;
                    case 'JUN':
                        $mes0='06';
                        break;
                    case 'JUL':
                        $mes0='07';
                        break;
                    case 'AGO':
                        $mes0='08';
                        break;
                    case 'SEP':
                        $mes0='09';
                        break;
                    case 'OCT':
                        $mes0='10';
                        break;
                    case 'NOV':
                        $mes0='11';
                        break;
                    case 'DIC':
                        $mes0='12';
                        break;
                };
            $fec_in_autorizacion=$fecautini[2]."-".$mes0."-".$fecautini[1];
            $fecfi_autorizacion=$nota['fec_fin_autorizacion'];
            $fecautfin= explode("-", $fecfi_autorizacion);
            $mesautfin=$fecautfin[0];
                switch ($mesautfin) {
                    case 'ENE':
                        $mes2='01';
                        break;
                    case 'FEB':
                        $mes2='02';
                        break;
                    case 'MAR':
                        $mes2='03';
                        break;
                    case 'ABR':
                        $mes2='04';
                        break;
                    case 'MAY':
                        $mes2='05';
                        break;
                    case 'JUN':
                        $mes2='06';
                        break;
                    case 'JUL':
                        $mes2='07';
                        break;
                    case 'AGO':
                        $mes2='08';
                        break;
                    case 'SEP':
                        $mes2='09';
                        break;
                    case 'OCT':
                        $mes2='10';
                        break;
                    case 'NOV':
                        $mes2='11';
                        break;
                    case 'DIC':
                        $mes2='12';
                        break;
                };
            $fec_fi_autorizacion=$fecautfin[2]."-".$mes2."-".$fecautfin[1];
            $prefijo_rango_nume=$nota['prefijo_rango_nume'];
            $rango_numeración_mín=$nota['rango_numeracion_min'];
            $rango_numeración_max=$nota['rango_numeracion_max'];
            $objetoXML->startElement("DRF");
                $objetoXML->startElement("DRF_1");//Número autorización: Número del código de la resolución otorgada para la numeración
                $objetoXML->text($numero_autorizacion);
                $objetoXML->endElement(); // Final del elemento
                $objetoXML->startElement("DRF_2");//Fecha de inicio del período de autorización de la numeración.
                $objetoXML->text($fec_in_autorizacion);
                $objetoXML->endElement(); // Final del elemento
                $objetoXML->startElement("DRF_3");//Fecha de fin del período de autorización de la numeración.
                $objetoXML->text($fec_fi_autorizacion);//
                $objetoXML->endElement(); // Final del elemento
                $objetoXML->startElement("DRF_4");//Prefijo del rango de numeración.
                $objetoXML->text($prefijo_rango_nume); //
                $objetoXML->endElement(); // Final del elemento
                $objetoXML->startElement("DRF_5");//Rango de Numeración (mínimo).
                $objetoXML->text($rango_numeración_mín);
                $objetoXML->endElement(); // Final del elemento
                $objetoXML->startElement("DRF_6");//Rango de Numeración (máximo).
                $objetoXML->text($rango_numeración_max);
                $objetoXML->endElement(); // Final del elemento
            $objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("REF");//Prefijo del rango de numeración.
				$objetoXML->startElement("REF_1");//Prefijo del rango de numeración.
				$objetoXML->text('IV'); //
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("REF_2");//Numero del documento que se referencia, asignado por el emisor..
				$objetoXML->text($nota->facturas->numero);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("REF_3");//Fecha de emisión.
				$objetoXML->text($fec_emision);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("REF_4");//CUFE/CUDE del documento referenciado.
				$objetoXML->text($cufe);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("REF_5");//Identificador del esquema de identificación..
				$objetoXML->text($ident_cufe);
				$objetoXML->endElement(); // Final del elemento
			$objetoXML->endElement(); // Final del elemento
			$cod_planilla='CGEN13';
			$objetoXML->startElement("CTS");
				$objetoXML->startElement("CTS_1");//Código de la plantilla que aplica para el comprobante.
				$objetoXML->text($cod_planilla);
				$objetoXML->endElement(); // Final del elemento
			$objetoXML->endElement(); // Final del elemento
			$objetoXML->startElement("CDN");
				$objetoXML->startElement("CDN_1");//Código del tipo de concepto por el cual se emite la nota.
				$objetoXML->text($nota['Cod_tip_concep']);
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->startElement("CDN_2");//Descripción de la naturaleza de la corrección. Tamaño mínimo 20 tamaño máximo 5000.
				$objetoXML->text($nota['motiv_devol']);
				$objetoXML->endElement(); // Final del elemento
			$objetoXML->endElement(); // Final del elemento
					$i=0;
					foreach($nota['detalles'] as $detalle){
						$i++;
						// dd($detalle['item']);
						$valor_impo=$detalle['cantidad']*$detalle['precio_unit'];
						// dd($valor_total);
						$valor_descuento_item=$valor_impo*$detalle['descuento']/100;
						$valor_base_item=$valor_impo-$valor_descuento_item;
						$valor_tributoI=$valor_base_item*$detalle['impuesto']/100;
						$total_item=$valor_base_item+$valor_tributoI;
						// dd($valor_tributo);
					$objetoXML->startElement("ITE");
						$objetoXML->startElement("ITE_1");
						$objetoXML->text($i);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_3");
						$objetoXML->text($detalle['cantidad']);
						$objetoXML->endElement(); // Final del elemento
						$ident_unidad=94;
						$objetoXML->startElement("ITE_4");
						$objetoXML->text($ident_unidad);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_5");
						$objetoXML->text($valor_base_item);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_6");
						$objetoXML->text($moneda_importe);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_7");//
						$objetoXML->text($detalle['precio_unit']);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_8");//Moneda del valor del artículo o servicio.
						$objetoXML->text($moneda_importe);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_10"); // informacion general de articulo.
						$objetoXML->text($detalle['descripcion']);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_11");//
						$objetoXML->text('');
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_19");//Total del ítem .
						$objetoXML->text($valor_base_item);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_20");//Moneda del Total del ítem (incluyendo Descuentos y cargos)
						$objetoXML->text($moneda_importe);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_21");//Valor a pagar del ítem
						$objetoXML->text($total_item );
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_22");//Moneda del Valor a pagar ítem. -DV del NIT del adquiriente
						$objetoXML->text($moneda_importe);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_27");//La cantidad real sobre la cual el precio aplica.
						$objetoXML->text($detalle['cantidad']);
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("ITE_28");//Unidad de medida de la cantidad del artículo solicitado
						$objetoXML->text('94');
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->startElement("IAE");
							$objetoXML->startElement("IAE_1");//Código del producto
							$objetoXML->text($detalle['item']);
							$objetoXML->endElement(); // Final del elemento
							$codigo_estandar='999';
							$Cod_schemeAgencyID='';
							$nombre_estandar='Estándar de adopción del contribuyente';
							$objetoXML->startElement("IAE_2");//Código del estándar.
							$objetoXML->text($codigo_estandar);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IAE_3");//Código "schemeAgencyID".
							$objetoXML->text($Cod_schemeAgencyID);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IAE_4");//Nombre del estándar de producto utilizado
							$objetoXML->text($nombre_estandar);
							$objetoXML->endElement(); // Final del elemento		$objetoXML->endElement(); // Final del elemento
						$objetoXML->endElement(); // Final del elemento
						if($detalle['descuento'] != 0 || $detalle['descuento'] != 0.0){
							$ident_descuento='false';
						$objetoXML->startElement("IDE");
							$objetoXML->startElement("IDE_1");//Indica que el elemento es un Cargo y no un descuento
							$objetoXML->text($ident_descuento);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IDE_2");//Valor total del cargo o descuento
							$objetoXML->text($valor_descuento_item);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IDE_3");//Moneda del valor total del cargo o descuento
							$objetoXML->text($moneda_importe);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IDE_5");//Razón (texto): Texto libre para informar de la razón del descuento.
							$objetoXML->text('descuento');
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IDE_6");//Porcentaje: Porcentaje que aplicar.
							$objetoXML->text($detalle['descuento']);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IDE_7"); // Valor Base para calcular el descuento el cargo.
							$objetoXML->text($valor_impo);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IDE_8");//Moneda del valor base para el calculo del descuento o cargo.
							$objetoXML->text($moneda_importe);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IDE_10");//Moneda del valor base para el calculo del descuento o cargo.
							$objetoXML->text('1');
							$objetoXML->endElement(); // Final del elemento
						$objetoXML->endElement(); // Final del elemento
						}else{

						}
						$objetoXML->startElement("TII");
							$objetoXML->startElement("TII_1");//Valor del tributo.
							$objetoXML->text($valor_tributoI);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("TII_2");//Moneda del valor del tributo
							$objetoXML->text($moneda_importe);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("TII_3");//
							$objetoXML->text("false");
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IIM");
							$objetoXML->startElement("IIM_1");// Identificador del tributo
							$objetoXML->text('01');
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IIM_2");//Valor del tributo
							$objetoXML->text($valor_tributoI);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IIM_3");//Moneda del valor del tributo
							$objetoXML->text($moneda_importe);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IIM_4");//Base Imponible sobre la que se calcula el valor del tributo del item
							$objetoXML->text($valor_base_item);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IIM_5");//Moneda de la base imponible
							$objetoXML->text($moneda_importe);
							$objetoXML->endElement(); // Final del elemento
							$objetoXML->startElement("IIM_6");//Tarifa del tributo.
							// $tarifa_impuesto='19.00';
							$objetoXML->text($tarifa_tributo);
							$objetoXML->endElement(); // Final del elemento
						$objetoXML->endElement(); // Final del elemento
						$objetoXML->endElement(); // Final del elemento

				$objetoXML->endElement(); // Final del elemento
			}
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->endElement(); // Final del elemento
				$objetoXML->fullEndElement (); // Final del elemento que cubre cada la matriz.

		$objetoXML->endElement(); // Final del nodo raíz, "obras"

		$objetoXML->endDocument(); // Final del documento
		// dd($objetoXML);
		// $xml= htmlentities($objetoXML->outputMemory());
		$xml = trim($objetoXML->outputMemory());
		// echo $objetoXML->flush();
		// dd($xml);
		$xmlBase64 = base64_encode($xml);
		// dd($xmlBase64);
return [
'xmlBase64'=> $xmlBase64,
'numero' =>$nota['numero'],
];

}
}
	// dd($xmlBase64);

	}
		}





