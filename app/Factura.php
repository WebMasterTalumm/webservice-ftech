<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    protected $table = "facturas";    

    public function detalles(){
        return $this->hasMany('App\DetalleFactura', 'id_factura');
    }
    public function municipios(){
        return $this->belongsTo('App\Municipio', 'id_municipio');
    }
    

    
}
