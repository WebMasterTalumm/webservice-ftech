<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleFactValidar extends Model
{
    protected $table='detalles_facturas_validar'; 

    public function facturas(){
        return $this->belongsTo('App\FacturaValidar', 'id_factura');
    }
}
