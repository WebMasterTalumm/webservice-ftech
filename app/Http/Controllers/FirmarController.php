<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factura;

class FirmarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Firmar()
    {
        $facturas=Factura::select('facturas.*')->wherenull('transaccionID')->get();
        // dd($facturas);
        foreach ($facturas as $factura) {
            $numero=$factura->numero;
            $dto='invoic';
            // dd($numero, $dto);
            return redirect()->action("SoapController2@FtechActionuploadInvoiceFile", [
                'numero'=> $numero,
                'dto' =>$dto,
            ]);
        }
    }

    public function ConsumirCufe()
    {
        $facturas=Factura::select('facturas.*')->wherenull('cufe')->WhereNotNull('transaccionID')->get();
        // dd($facturas);
        foreach ($facturas as $factura) {
            $numero=$factura->numero;
            $dto='invoic';
            // dd($numero, $dto);
            return redirect()->action("SoapController2@FtechActiongetCUFEFile", [
                'numero'=> $numero,
            ]);
        }
    }
}
