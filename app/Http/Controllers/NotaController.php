<?php

namespace App\Http\Controllers;

use App\Factura;
use App\DetalleFactura;
use App\LogImportacion;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\DB;
use File;
use App\Nota;
use App\Adquiriente;
use App\Municipio;
use App\DetalleNota;
use App\PrefijoNumeracion;
use App\Contacto;
use App\LogImportacionNotas;
use App\ControlUpload;
use Illuminate\Support\Facades\Auth;
use App\User;


class NotaController extends Controller
{
    public function importar()
    {
        return view('subir_comprobantes.notas');
    }
    public function fileupload(Request $request){

        if($request->hasFile('file')) {

          // Upload path
        //   dd($request);
        //   $destinationPath = 'notas/';
           $id_usuario = Auth::user()->id;
            $destinationPath = 'notas/'.$id_usuario.'/';
        //   $filenam = $carpeta.'facturas/'.$pdf->getFilename();
        //   dd($filename);

          // Create directory if not exists
          if (!file_exists($destinationPath)) {
            // dd($destinationPath);
            //  exit("la factura ya existe");
             mkdir($destinationPath, 0755, true);
          }
          // Get file extension
            $extension = $request->file('file')->getClientOriginalExtension();
            $name = pathinfo($request->file('file')->getClientOriginalName(),PATHINFO_FILENAME);

            $validextensions = array("jpeg","jpg","png","pdf");

            // Check extension
            if(in_array(strtolower($extension), $validextensions)){

                // $usuario = Auth::user()->id;
                $fileName = $name.'.' . $extension;
                $nota= ControlUpload::select('control_upload.*')->where('filename','=',$fileName)->first();
                if (empty($factura)) {

                    $request->file('file')->move($destinationPath, $fileName);
                        $projectImage = new ControlUpload();
                        $projectImage->usuario = $id_usuario;
                        $projectImage->filename = $fileName;
                        $projectImage->save();
                }else{
                    exit("la factura ya existe");
                }
                }
        }
     }
    /**
     * descripcion:  lee el archivo del  la carpeta y lo guarda en la base de datos y elimina
     * el archivo de la carpeta
     * parametros: null
     * return: el arreglo del estado del archivo de la carpeta
     * @return \Illuminate\Http\Response
     */
    public function importFacturas()
    {
        $id_usuario = Auth::user()->id;
        $carpeta = 'notas/'.$id_usuario.'/';
        $pdfs = File::files(public_path().'/'.$carpeta);
        
        // $carpeta = 'facturas';
        if (empty($pdfs)) {
            $msg=Nota::select('numero','razon_social','cude','estado_envio')->take(5)
            ->orderBy('id' , 'desc')
            ->get();

            return redirect()->back()->with(compact('msg'));
        }

        $resultado = [];
        foreach ($pdfs as $pdf) {
            $filename = $carpeta.'/'.$pdf->getFilename();

            $parseador = new \Smalot\PdfParser\Parser();
            $documento = $parseador->parseFile($filename);
            
            $paginas=$documento->getPages();
            // dd($paginas);
            $size=sizeof($paginas);
            $i=0;
            foreach ($paginas as $page) {
            $texto=$page->getText();
            // dd($texto);
            $seccionespdf= $this->pdfToArray($texto);
            // dd($seccionespdf);
            // $seccionespdf= $this->pdfToArray($filename);
            $propiedades=['Nt_CRD','fecha','fecha de vencimiento','cliente','contacto','nit o c.c','direccion','ciudad','telefono','rte fuente','impoconsumo','rte iva','rte ica','rte cree','Observacion','Motivo Devol','Vendedor','Sucursal'];
            $atributos = $this->readFromPdf($propiedades,$seccionespdf);
            // dd($atributos);

            $productos= $this->getproductos($seccionespdf);
            // dd($productos);

            $verificacion= $this->storeFromFile($atributos, $productos, $filename);
            }
            if ($verificacion) {
                $eliminacion = File::delete($filename);
                if ($eliminacion) {
                    $resultado[] = "archivo ".$filename." guardado. eliminacion satisfatoria";
                        $num=explode('-',$atributos['Nt_CRD']);
                        $numero=$num[1].intval($num[2]);
                    return redirect()->action("SoapController@Transmitir", [
                        'numero'=> $numero,
                        'dto' =>'Nota',
                    ]);
                }else {
                    $resultado[] = "archivo ".$filename." guardado. eliminacion fallida";
                }
            }else {
                $resultado[] = "Error: archivo ".$filename."No guardado";
            }
        }
        return $resultado;
    }
    /**
     * DESCRIPCION: toma el archivo de la carpeta lo divide por secciones separadas
     * por + y elimina las posiciones que solo tiene -- y vacias
     * PARAMETROS: el nombre del archivo
     * VALORES DE RETORNO: el array del archivo ya separa por secciones y limpio
     */
    private function pdfToArray($texto){

        // $texto = $documento->getText();
        // dd($texto);
        $seccionespdf = explode('+', $texto);
        $array1=trim($seccionespdf[0]);
        $array2=trim($seccionespdf[6]);
        // dd($array1);
        $valor1= "DOSPrinter 3.4 DEMO";
        $valor2="importadorasurtihogarsas2011@gmail.com ";
        if ($array1 == $valor1) {
            unset($seccionespdf[0]);
            // $secciones1pdf = asort($seccionespdf);
        }
        if ($array2 == $valor2) {
            unset($seccionespdf[6]);
            // $secciones1pdf = asort($seccionespdf);
        }
        $seccionespdf = array_values(array_filter($seccionespdf, function($seccion){
            $pos = strpos($seccion,"--");
            return !(($pos !== false) || (empty(trim($seccion)))) ;
        }));
        // dd($seccionespdf);
        return $seccionespdf;


    }
    private function multiexplode ($delimiters,$data) {
        $MakeReady = str_replace($delimiters, $delimiters[0], $data);
        $Return    = explode($delimiters[0], $MakeReady);
        return  $Return;
    }
    /**
     * DESCRIPCION: toma el arreglo de la propiedades que necesito y el pdf separado por +
     * y haciendo uso de la funcion getproperty retorna un arreglo con todas las propiedades solicitadas
     * PARAMETROS: las propiedades y las secciones separadas por +
     * VALORES DE RETORNO: arreglo con los valores de las propiedades
     */
    private function readFromPdf($p_propiedades, $p_seccionespdf){
        $resultado=[];
        foreach ($p_seccionespdf as $seccionpdf) {
            // $seccionpdf = explode('  ', $seccionpdf);
            $seccionpdf = explode('|', $seccionpdf);
            foreach ($p_propiedades as $propiedad) {
                $consulta = $this->getproperty($seccionpdf,$propiedad);
                if (!empty($consulta)) {
                    $resultado[$propiedad] = $consulta;

                }
            }

        }


        return $resultado;
    }

    private function storeFromFile($atributos,$productos,$nombrearchivo){
        DB::beginTransaction();
        try{
            //  dd($atributos);
            if (array_key_exists('Sucursal', $atributos)) {
                 $sepa= explode('Factura',$atributos['Sucursal']);
            }else{
                 $sepa= explode('Factura',$atributos['cliente']);
            }
             
            //  dd($sepa);
             $factur=explode(':',$sepa[1]);
            //  dd($factur);
            $fact=trim($factur[1]);
        //   dd($fact);
            $num_fact=explode('-',$fact);
            $pref_fact=$num_fact[0];
            $num_fact=$pref_fact.'VT'.intval($num_fact[1]);
            // dd($num_fact);
            $num=explode('-',$atributos['Nt_CRD']);
            $numero=$num[1].intval($num[2]);
            // dd($numero);
            // $numero='CONT2';
            $prefijo=$num[1];
            // dd($prefijo);
            $nit = array_filter($this->multiexplode(array("Fecha Vcto"),$atributos['nit o c.c']));
             sort($nit);
             $vencimiento = str_replace(":", "", $nit[1]);
            //   dd($nit);
             $direccion=explode('Vendedor',$atributos['direccion']);
            //  dd($direccion);
             $event=explode(":",$direccion[1]);
            //  dd($event);
             $vendedor=trim($event[1]);
             $fact= Nota::where('numero','=',$numero)->first();

            //  $ultimo=Nota::select('notas.*')->get()->last();
            //  // dd($fact,$ultimo);
            //  $ultnum=substr($ultimo->numero,4);
            //  $ultpre=substr($ultimo->numero,0,4);
            //  $proxnum=$ultnum+1;
            //  $prox=$ultpre.$proxnum;

            //  if ($prox != $numero) {
            //      $msg='se espera el comprobante numero'.$prox;
            //      echo '<div class="col-lg-12">
            //      <a href="javascript:history.back(-1);" title="Ir la página anterior"><button class="button">';
            //              echo'Regresar';
            //              echo'</button></a>
            //          </div><br>';
            //      exit('se espera el comprobante numero'.($prox));
            //  }
             
            //  $facturas= Factura::select('facturas.*')->where('numero','=',$factura[1])->first();
             $facturas= Factura::select('facturas.*')->where('numero','=',trim($num_fact))->first();
            //  dd($facturas);
             if(!empty($facturas)){
                 $id_factura=$facturas->id;
                //  dd($facturas);
             }else {
                 $id_factura=0;
             }
            $notas= Nota::select('notas.*')->where('numero','=',$numero)->first();
            
        if (empty($notas)) {
        //   dd($notas);
        
             $nota= new Nota;
             $nota->tipo_contacto = $facturas->tipo_contacto;
             $nota->nombre_contacto = $facturas->nombre_contacto;
             $nota->cargo_contacto = $facturas->cargo_contacto;
             $nota->telefono_contacto = $facturas->telefono_contacto;
             $nota->email_contacto = $facturas->email_contacto;
             $nota->ident_tip_pers = $facturas->ident_tip_pers;
             $nota->identifi_adqui = $facturas->identifi_adqui;
             $nota->tip_identifi = $facturas->tip_identifi;
             $nota->regimen = $facturas->regimen;
             $nota->razon_social = $facturas->razon_social;
             $nota->nombre_adqui = $facturas->nombre_adqui;
             $nota->apellidos_adqui = $facturas->apellidos_adqui;
             $nota->direccion = $facturas->direccion;
             $nota->nom_regis_rut = $facturas->nom_regis_rut;
             $nota->cod_postal = $facturas->cod_postal;
             $nota->id_municipio = $facturas->id_municipio;
             $nota->telefono = $facturas->telefono;
             $nota->responsa_adqui = $facturas->responsa_adqui;
             $nota->ident_tributo =$facturas->ident_tributo;
             $nota->nom_tributo =$facturas->nom_tributo;
             $nota->cod_med_pago = $facturas->cod_med_pago;
             $nota->metodo_pago =$facturas->cod_med_pago;
             $nota->fecha = $atributos['fecha'];
             $nota->numero = $numero;
             $num_not=explode("-",$numero);
             $nota->folio = intval($num[2]);
                //  $nota->numero = $atributos['Nt_CRD'];
                //  $nota->factura =  $factura[1];
             $nota->factura =  $num_fact;
             $nota->id_factura =  $id_factura;
             $nota->vendedor=$vendedor;
             @$nota->fecha_vencimiento = trim($vencimiento);
             @$nota->rte_fuente = intval(str_replace(',','',$atributos['rte fuente']));
             @$nota->impoconsumo = intval(str_replace(',','',$atributos['impoconsumo']));
             @$nota->rte_iva = intval(str_replace(',','',$atributos['rte iva']));
             @$nota->rte_ica = intval(str_replace(',','',$atributos['rte ica']));


             
             $numeracion_au= PrefijoNumeracion::select('prefijos_numeracion.*')->where('prefijo','like', '%' .$prefijo.'%')->first();
            //  dd($numeracion_au);
             $nota->rango_prefijo=$numeracion_au->rango_prefijo;
             $nota->num_autorizacion=$numeracion_au->autorizacion;
             $nota->fec_ini_autorizacion=$numeracion_au->fecha_inicio;
             $nota->fec_fin_autorizacion=$numeracion_au->fecha_fin;
             $nota->prefijo_rango_nume=$numeracion_au->prefijo;
             $nota->rango_numeracion_max=$numeracion_au->numero_final;
             $nota->rango_numeracion_min=$numeracion_au->numero_inicia;
             $nota->tipo_documento=$numeracion_au->tipo_documento;
             if($numeracion_au->tipo_documento == 91){
                $nota->tipo_operacion=20; 
             }elseif($numeracion_au->tipo_documento == 92){
                $nota->tipo_operacion=30; 
             }
            //  $nota->folio=$numeracion_au->numero_inicia;

             
             $motivo_devol=explode(' ',$atributos['Motivo Devol']);
             $nota->motiv_devol =$atributos['Motivo Devol'];
             if($motivo_devol[0] == '01'){
                 $nota->Cod_tip_concep = 2;
             }elseif($motivo_devol[0] == '02'){
                 $nota->Cod_tip_concep = 1;
             }elseif($motivo_devol[0] == '03' || $motivo_devol[0] == '04'){
                 $nota->Cod_tip_concep = 2;
             }
             if (array_key_exists('Observacion', $atributos)) {
                 $observacion=$atributos['Observacion'];
            }else{
                 $observacion=$atributos['Motivo Devol'];
            }
            $nota->observaciones=$observacion;
            $nota->estado=0;
            // dd($nota);
            $nota->save();
            // dd($nota);
             $id_nota=$nota->id;
             $notas= Nota::select('id')->where('id','=',$id_nota)->Where('numero', 'like', '%' . 'NC' . '%')->first();
            //  dd($notas);
             if ($numeracion_au->tipo == 'nc') {
                $not = Nota::find($id_nota);
                $not->tipo_documento=91;
                $not->tipo_operacion=20;
                $not->save();
             }elseif($numeracion_au->tipo == 'nd'){
                $not = Nota::find($id_nota);
                $not->tipo_documento=92;
                $not->tipo_operacion=30;
                $not->save();
             }
            foreach ($productos as $producto) {
                $detalle = new DetalleNota;
                $detalle->id_nota = $id_nota;
                $detalle->item = $producto['referencia'];
                $detalle->descripcion =$producto['descripcion'];
                $detal=explode(" ",$producto['cantidad']);
                // dd($detal);
                $detalle->cantidad =$detal[0];
                
                // $detalle->cantidad =$producto['cantidad'];
                if ($detal[1] == 'UND') {
                   $um=94;
                }else{
                    $um=$detal[1];
                }
                $detalle->um =$um;
                @$detalle->precio_unit =intval(str_replace(',','',$producto['precio_unit']));
                @$detalle->descuento =$producto['descuento'];
                @$detalle->valor_total =intval(str_replace(',','',$producto['valor_total']));
                @$detalle->impuesto =$producto['impuesto'];
                $detalle->save();
            }
            // dd($detalle);
            $log = new LogImportacionNotas;
            $log->id_nota=$nota->id;
            $log->filename=$nombrearchivo;
            $log->save();
        }else{
            foreach ($productos as $producto) {
                $detalle = new DetalleNota;
                $detalle->id_nota = $notas->id;
                $detalle->item = $producto['referencia'];
                $detalle->descripcion =$producto['descripcion'];
                $detal=explode(" ",$producto['cantidad']);
                // dd($detal);
                $detalle->cantidad =$detal[0];
                
                // $detalle->cantidad =$producto['cantidad'];
                if ($detal[1] == 'UND') {
                   $um=94;
                }else{
                    $um=$detal[1];
                }
                $detalle->um =$um;
                @$detalle->precio_unit =intval(str_replace(',','',$producto['precio_unit']));
                @$detalle->descuento =$producto['descuento'];
                @$detalle->valor_total =intval(str_replace(',','',$producto['valor_total']));
                @$detalle->impuesto =$producto['impuesto'];
                $detalle->save();
            }
        }
            DB::commit();
            return true;
        }catch(\Excepcion $e){
            return false;
        }
    }

    /**
     * descripcion:esta funcion recibe como parametro el pdf separados por | y la propiedad que quiero
     * y retorna el valor de la propiedad que pedi
     * parametros: pdf=array | ,propiedad=string
     * return: matriz de dos dimensiones de valores
     */
    private function getproperty($pdf,$propiedad){
        $resultado = [];
        if (is_array($pdf)) {
            foreach ($pdf as $lineapdf)
            {
                $valorlinea = explode(":", $lineapdf,2);

                $tamanolinea = sizeof($valorlinea);

                if ($tamanolinea > 1 ) {

                    $pos = strpos(strtolower($valorlinea[0]), strtolower($propiedad));
                    // dd($pos);
                    if ($pos !== false) {
                        $resultado = trim($valorlinea[1]);
                    }

                }
            }
        }

        return $resultado;
    }

    /**
     * descripcion: me trae todos los detalles de todos los $productoss
     * parametros: todas secciones separadas por +
     * return: matriz de los productos
     */
    private function getproductos($seccionespdf)
    {
        // dd($seccionespdf);
        $resultado = [];
        foreach ($seccionespdf as $index => $seccionpdf) {
            // dd($seccionpdf);
            $pos = strpos($seccionpdf,":");
            if ($pos === false) {
                $listadoproductos= explode("|",$seccionespdf[$index+1]);
                // dd($listadoproductos);
                $listadoproductos = array_filter($listadoproductos, function($linea){
                    return !empty(trim($linea));
                });
                $cabeceras = ['referencia', 'descripcion','cantidad', 'precio_unit', 'descuento', 'valor_total','impuesto'];
                $attr_cabecera = 0;
                // dd($listadoproductos);
                foreach ($listadoproductos as $listadoproducto) {

                    $temp_cabecera= $cabeceras;
                    $listadoproducto = explode('  ',$listadoproducto);
                    $listadoproducto = array_filter($listadoproducto, function($linea){
                        return !empty(trim($linea));
                    });
                    $cantidad_atributos =sizeof($listadoproducto);
                    $listadoproducto = array_filter($listadoproducto, function($linea) use($cantidad_atributos){
                        return $cantidad_atributos>1;
                    });

                    $trimed= array_walk($listadoproducto,function (&$string, $index){
                        $string = trim($string);
                    });
                    $listadoproducto =array_values($listadoproducto);

                    // dd($listadoproducto);
                    if (sizeOf($listadoproducto) == 7 || sizeOf($listadoproducto) == 8) {
                        unset($listadoproducto[2]);
                    }
                    if (sizeOf($listadoproducto) == 6) {
                        $listadoproducto =array_values($listadoproducto);
                        $explo=explode(" ",$listadoproducto[5]);
                        $array= array_push($listadoproducto,$explo[0],$explo[1]);
                        unset($listadoproducto[5]);
                    }

                    $listadoproducto =array_values($listadoproducto);
                    if (sizeOf($listadoproducto) == 2) {
                        $listadoproducto=[];
                    }
                    if (sizeOf($listadoproducto) == 3) {
                        $listadoproducto=[];
                    }

                    if(!empty($listadoproducto)){
                        // dd($listadoproducto);
                        $resultado[] = array_combine($temp_cabecera,$listadoproducto);
                    }
                }
            }
        }
        // dd($resultado);
        return $resultado;
    }


}


