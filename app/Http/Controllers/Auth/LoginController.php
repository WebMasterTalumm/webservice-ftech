<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm(){
        return view('auth.login');
    }

    public function login(Request $request){
        $this->validateLogin($request);
        // dd($request);

        if (Auth::attempt(['identificacion' => $request->identificacion,'password' => $request->password])){
            return redirect()->route('home');
        }

        return back()
        ->withErrors(['identificacion' => trans('auth.failed')])
        ->withInput(request(['identificacion']));

    }

    protected function validateLogin(Request $request){
        $this->validate($request,[
            'identificacion' => 'required|string',
            'password' => 'required|string'
        ]);

    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
    }
}
