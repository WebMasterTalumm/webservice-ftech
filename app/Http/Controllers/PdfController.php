<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use QrCode;

class PdfController extends Controller
{
    public function invoice() 
    {
        $data = $this->getData();
        // $date = date('Y-m-d');
        // $invoice = "2222";
        $qr = \QrCode::size(200)->backgroundColor(255,55,0)->generate('W3Adda Laravel Tutorial');
        $view =  \View::make('pdf.invoice', compact('data'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        
        return $pdf->stream('invoice');
        // return view('pdf.invoice', compact('qr'));
    }
 
    public function getData() 
    {
        $data =  [
            'quantity'      => '1' ,
            'description'   => 'some ramdom text',
            'price'   => '500',
            'total'     => '500'
        ];
        return $data;
    }
    public function invoice2() 
    {
        $data = $this->getData();
        // $date = date('Y-m-d');
        // $invoice = "2222";
        $view =  \View::make('pdf.invoice1')->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('invoice');
    }
    public function invoice3() 
    {
        $data = $this->getData();
        // $date = date('Y-m-d');
        // $invoice = "2222";
        $view =  \View::make('pdf.invoice3')->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('invoice');
    }

}
