<?php

namespace App\Http\Controllers;

use App\Factura;
use App\DetalleFactura;
use App\LogImportacion;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\DB;
use File;
use App\Nota;
use App\Adquiriente;
use App\Municipio;
use App\DetalleNota;
use App\LogImportacionNotas;


class Nota1Controller extends Controller
{
    /**
     * descripcion:  lee el archivo del  la carpeta y lo guarda en la base de datos y elimina 
     * el archivo de la carpeta 
     * parametros: null
     * return: el arreglo del estado del archivo de la carpeta 
     * @return \Illuminate\Http\Response
     */
    public function importFacturas()
    {
        $carpeta = 'notas';
        $pdfs = File::files(public_path().'/'.$carpeta);
        $resultado = [];
        foreach ($pdfs as $pdf) {
            $filename = $carpeta.'/'.$pdf->getFilename();
            $seccionespdf= $this->pdfToArray($filename);            
            $propiedades=['Nt_CRD','fecha','fecha de vencimiento','cliente','contacto','nit o c.c','direccion','ciudad','telefono','rte fuente','impoconsumo','rte iva','rte ica','rte cree','Observacion','Motivo Devol'];    
            $atributos = $this->readFromPdf($propiedades,$seccionespdf);
            // dd($atributos);
            
            $productos= $this->getproductos($seccionespdf);
            // dd($productos);
          
            $verificacion= $this->storeFromFile($atributos, $productos, $filename);
            
            if ($verificacion) {
                $eliminacion = File::delete($filename);
                if ($eliminacion) {
                    $resultado[] = "archivo ".$filename." guardado. eliminacion satisfatoria";
                    return redirect()->action("SoapController@FtechActionuploadInvoiceFile", [
                        'numero'=> $atributos['Nt_CRD'],
                        'dto' =>'NC',
                    ]);
                }else {
                    $resultado[] = "archivo ".$filename." guardado. eliminacion fallida";
                }
            }else {
                $resultado[] = "Error: archivo ".$filename."No guardado";
            }            
        } 
        return $resultado;      
    }
    /**
     * DESCRIPCION: toma el archivo de la carpeta lo divide por secciones separadas 
     * por + y elimina las posiciones que solo tiene -- y vacias
     * PARAMETROS: el nombre del archivo 
     * VALORES DE RETORNO: el array del archivo ya separa por secciones y limpio 
     */
    private function pdfToArray($namepdf){
        $parseador = new \Smalot\PdfParser\Parser();
        $documento = $parseador->parseFile($namepdf);
        
        $texto = $documento->getText();
        // dd($texto);
        $seccionespdf = explode('+', $texto);
        $seccionespdf = array_values(array_filter($seccionespdf, function($seccion){
            $pos = strpos($seccion,"--");
            return !(($pos !== false) || (empty(trim($seccion)))) ;
        }));
        // dd($seccionespdf);
        return $seccionespdf;
       
        
    }
    private function multiexplode ($delimiters,$data) {
        $MakeReady = str_replace($delimiters, $delimiters[0], $data);
        $Return    = explode($delimiters[0], $MakeReady);
        return  $Return;
    }
    /**
     * DESCRIPCION: toma el arreglo de la propiedades que necesito y el pdf separado por + 
     * y haciendo uso de la funcion getproperty retorna un arreglo con todas las propiedades solicitadas 
     * PARAMETROS: las propiedades y las secciones separadas por +
     * VALORES DE RETORNO: arreglo con los valores de las propiedades 
     */
    private function readFromPdf($p_propiedades, $p_seccionespdf){
        $resultado=[];
        foreach ($p_seccionespdf as $seccionpdf) {
            // $seccionpdf = explode('  ', $seccionpdf);
            $seccionpdf = explode('|', $seccionpdf);
            foreach ($p_propiedades as $propiedad) {
                $consulta = $this->getproperty($seccionpdf,$propiedad);
                if (!empty($consulta)) {
                    $resultado[$propiedad] = $consulta;   
                               
                }
            }
            
        }
                    
        
        return $resultado;
    }
    
    private function storeFromFile($atributos,$productos,$nombrearchivo){
        DB::beginTransaction();
        try{
             // dd($atributos,$productos);
             $sepa= explode('                                  ',$atributos['cliente']);
             $factura=explode(':',$sepa[1]);
             $nit = array_filter($this->multiexplode(array(":","                     "),$atributos['nit o c.c']));
             sort($nit);
             $direccion=explode('                                  ',$atributos['direccion']);
            //  dd($direccion);
             $fact= Factura::where('numero','=',$atributos['Nt_CRD'])->first();
             
             if (!empty($fact)) {
 
                 exit("la factura ($nombrearchivo) ya existe");
                 
             }
             $facturas= Factura::select('facturas.*')->where('numero','=',$factura[1])->first();
             if(!empty($id_factura)){
                 $id_factura=$factura->id;
             }else {
                 $id_factura=0;
             }
 
             $nota= new Nota;
             $nota->numero = $atributos['Nt_CRD'];
             $nota->fecha = $atributos['fecha'];
             $nota->factura =  $factura[1];
             $nota->id_factura =  $id_factura;
             @$nota->fecha_vencimiento = $nit[0];
             @$nota->rte_fuente = intval(str_replace(',','',$atributos['rte fuente']));
             @$nota->impoconsumo = intval(str_replace(',','',$atributos['impoconsumo']));
             @$nota->rte_iva = intval(str_replace(',','',$atributos['rte iva']));
             @$nota->rte_ica = intval(str_replace(',','',$atributos['rte ica']));
             $nota->tipo_contacto=1;
             $nota->nombre_contacto=$sepa[0];
             $nota->cargo_contacto=' ';
             // $telefono=explode(" ",$atributos['telefono']);
             // dd($telefono[0]);
             $nota->telefono_contacto=$atributos['telefono'];
             $nota->email_contacto=' ';
             $nit_adquiriente=explode(" ",$nit[2]);
             // dd($nit_adquiriente);
             $nota->identifi_adqui =$nit_adquiriente[0];
             $ejemplo = explode("-", $nit_adquiriente[0]);
             $cant=strlen($ejemplo[0]);
             $p_digito=substr($ejemplo[0],0,1);
             // dd($p_digito);
             if ($p_digito == 9 || $p_digito == 8 && strlen($ejemplo[0]) == 9 ) {
                 // dd($nit_adquiriente);
                 $tip_identifi=31; 
                 $ident_tip_pers=1;
                 $razon_social=$sepa[0];
                 $nombre_adqui=$sepa[0];
                 $apellido_adqui=' ';
             }else{
                 // dd($nit_adquiriente);
                 $tip_identifi=13;
                 $ident_tip_pers=2;
                 $razon_social=$sepa[0];
                 $cant1 = count(explode(" ", $razon_social));
                 $nomb_apelli=explode(" ",$razon_social);
                 // dd($cant1);
                 if($cant1 == 4){
                     $nombre_adqui= $nomb_apelli[2]." ".$nomb_apelli[3];
                     $apellido_adqui= $nomb_apelli[0]." ".$nomb_apelli[1];
                     // dd($apellido_adqui);
                     
                 }else{
                     $nombre_adqui= $nomb_apelli[2];
                     $apellido_adqui= $nomb_apelli[0]." ".$nomb_apelli[1];
                     // dd($apellido_adqui);
                 }
                 // $nombre=explode(" ",$atributos['cliente']);
                 // $nombre_adqui= $nombre[0];
             }
             // dd($apellido_adqui);
             @$nota->ident_tip_pers =$ident_tip_pers;
             $nota->tip_identifi =$tip_identifi;
             $nota->regimen =0;
             $nota->razon_social = $razon_social;
             $nota->nombre_adqui = $nombre_adqui;
             $nota->apellidos_adqui = $apellido_adqui;
             $nota->direccion = $direccion[0];
             $nota->nom_regis_rut = $razon_social;
             $municipio=explode('                                                    ',$atributos['ciudad']);
             $inf_municipio= Municipio::select('municipios.*')->where('nombre_municipio','=',$municipio[0])->first();
             @$id_municipio=$inf_municipio->id;
             $nota->id_municipio = $id_municipio;
             $nota->responsa_adqui = '';
             $nota->ident_tributo = '01';
             $nota->nom_tributo = 'IVA';
             $motivo_devol=explode(' ',$atributos['Motivo Devol']);
             $nota->motiv_devol =$atributos['Motivo Devol'];
             // dd($motivo_devol);
             if($motivo_devol[0] == '01'){
                 $nota->Cod_tip_concep = 2;
             }elseif($motivo_devol[0] == '02'){
                 $nota->Cod_tip_concep = 1;
             }elseif($motivo_devol[0] == '03' || $motivo_devol[0] == '04'){
                 $nota->Cod_tip_concep = 5;
             }
             $nota->save();
             $id_nota=$nota->id;
             $notas= Nota::select('id')->Where('numero', 'like', '%' . 'NC' . '%')->first();
             $not = Nota::find($notas->id);
             $not->tipo_documento=91;
             $not->tipo_operacion=20;
             $not->save();
            //  dd($nota , $not);
             $adqui = Adquiriente::where('identifi_adqui','=',$nit_adquiriente[0])->first();
                 if(empty($adqui)){
                     $adquiriente=new Adquiriente;
                     $adquiriente->ident_tip_pers= $ident_tip_pers;
                     $adquiriente->identifi_adqui =$nit_adquiriente[0];
                     $adquiriente->tip_identifi =$tip_identifi;
                     $adquiriente->regimen =0;
                     $adquiriente->razon_social = $razon_social;
                     $adquiriente->nombre_adqui = $nombre_adqui;
                     $adquiriente->apellidos_adqui = $apellido_adqui;
                     $adquiriente->direccion = $atributos['direccion'];
                     $adquiriente->nom_regis_rut = $razon_social;
                     @$adquiriente->id_municipio = @$id_municipio;
                     $adquiriente->responsa_adqui = '';
                     $adquiriente->telefono = $atributos['telefono'];
                     $adquiriente->ident_tributo = '01';
                     $adquiriente->nom_tributo = 'IVA';
                     $adquiriente->save();
                 }
            foreach ($productos as $producto) {
                // dd($producto);
                // die();
                // dd($id_nota);
                $detalle = new DetalleNota;
                $detalle->id_nota = $id_nota;
                $detalle->item = $producto['referencia'];
                $detalle->descripcion =$producto['descripcion'];
                // $detalle->mo = @$$productos['mo'] ? $$productos['mo']:0 ;
                $detalle->cantidad =$producto['cantidad'];
                // $detalle->um =$$productos['um'];
                @$detalle->precio_unit =intval(str_replace(',','',$producto['precio_unit']));
                @$detalle->descuento =$producto['descuento'];
                @$detalle->valor_total =intval(str_replace(',','',$producto['valor_total']));
                @$detalle->impuesto =$producto['impuesto'];
                $detalle->save();           
            }
            $log = new LogImportacionNotas;
            $log->id_nota=$nota->id;
            $log->filename=$nombrearchivo;
            $log->save();

            DB::commit();
            return true;            
        }catch(\Excepcion $e){
            return false;
        }   
    }
    
    /**
     * descripcion:esta funcion recibe como parametro el pdf separados por | y la propiedad que quiero 
     * y retorna el valor de la propiedad que pedi
     * parametros: pdf=array | ,propiedad=string
     * return: matriz de dos dimensiones de valores
     */
    private function getproperty($pdf,$propiedad){
        $resultado = [];
        if (is_array($pdf)) {
            foreach ($pdf as $lineapdf)
            {
                $valorlinea = explode(":", $lineapdf,2);
              
                $tamanolinea = sizeof($valorlinea);  

                if ($tamanolinea > 1 ) {
                    
                    $pos = strpos(strtolower($valorlinea[0]), strtolower($propiedad));
                    // dd($pos);
                    if ($pos !== false) {
                        $resultado = trim($valorlinea[1]);  
                    }
                    
                } 
            }
        }
        
        return $resultado;
    }

    /**
     * descripcion: me trae todos los detalles de todos los $productoss  
     * parametros: todas secciones separadas por +
     * return: matriz de los productos 
     */
    private function getproductos($seccionespdf)
    {
        // dd($seccionespdf);
        $resultado = [];
        foreach ($seccionespdf as $index => $seccionpdf) {
            // dd($seccionpdf);
            $pos = strpos($seccionpdf,":");
            if ($pos === false) {
                $listadoproductos= explode("|",$seccionespdf[$index+1]);
                // dd($listadoproductos);
                $listadoproductos = array_filter($listadoproductos, function($linea){            
                    return !empty(trim($linea));
                });
                $cabeceras = ['referencia', 'descripcion','cantidad', 'precio_unit', 'descuento', 'valor_total','impuesto'];
                $attr_cabecera = 0;
                // dd($listadoproductos);
                foreach ($listadoproductos as $listadoproducto) {
                   
                    $temp_cabecera= $cabeceras;
                    $listadoproducto = explode('  ',$listadoproducto);            
                    $listadoproducto = array_filter($listadoproducto, function($linea){            
                        return !empty(trim($linea));                
                    });
                    $cantidad_atributos =sizeof($listadoproducto);
                    $listadoproducto = array_filter($listadoproducto, function($linea) use($cantidad_atributos){            
                        return $cantidad_atributos>1;                
                    });
                   
                    $trimed= array_walk($listadoproducto,function (&$string, $index){
                        $string = trim($string);
                    });
                    $listadoproducto =array_values($listadoproducto);

                    // dd($listadoproducto);
                    if (sizeOf($listadoproducto) == 7 || sizeOf($listadoproducto) == 8) {
                        unset($listadoproducto[2]);
                    }
                    if (sizeOf($listadoproducto) == 6) {
                        $listadoproducto =array_values($listadoproducto);
                        $explo=explode(" ",$listadoproducto[5]);
                        $array= array_push($listadoproducto,$explo[0],$explo[1]);
                        unset($listadoproducto[5]);
                    }
                    
                    $listadoproducto =array_values($listadoproducto);
                    if (sizeOf($listadoproducto) == 2) {
                        $listadoproducto=[];
                    }
                    
                    if(!empty($listadoproducto)){
                        // dd($listadoproducto);
                        $resultado[] = array_combine($temp_cabecera,$listadoproducto);
                    }
                }                  
            }
        }
        // dd($resultado);
        return $resultado;
    }
    
   
}
    
   
