<?php

namespace App\Http\Controllers;

use File;
use App\User;
use DateTime;
use App\Factura;
use App\Contacto;
use App\Producto;
use App\Municipio;
use Carbon\Carbon;
use App\Adquiriente;
use App\ControlUpload;
use App\DetalleFactura;
use App\FacturaValidar;
use App\LogImportacion;
use App\PrefijoNumeracion;
use App\DetalleFactValidar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class FacturaController extends Controller
{


    public function importar()
    {
        return view('subir_comprobantes.facturas');
    }
    public function fileupload(Request $request){

        if($request->hasFile('file')) {
        
            $id_usuario = Auth::user()->id;
            $destinationPath = 'facturas/'.$id_usuario.'/';
    
          // Create directory if not exists
          if (!file_exists($destinationPath)) {
             mkdir($destinationPath, 0755, true);
          }
          // Get file extension
            $extension = $request->file('file')->getClientOriginalExtension();
            $name = pathinfo($request->file('file')->getClientOriginalName(),PATHINFO_FILENAME);

            $validextensions = array("jpeg","jpg","png","pdf",".TXT",".txt");

            // Check extension
            if(in_array(strtolower($extension), $validextensions)){

                // $usuario = Auth::user()->id;
                $fileName = $name.'.' . $extension;
                $factura= ControlUpload::select('control_upload.*')->where('filename','=',$fileName)->first();
                // if (empty($factura)) {

                    $request->file('file')->move($destinationPath, $fileName);
                        $projectImage = new ControlUpload();
                        $projectImage->usuario = $id_usuario;
                        $projectImage->filename = $fileName;
                        $projectImage->save();
                // }
                // else{
                //     exit("la factura ya existe");
                // }
            }
        }
    }
    /**
     * descripcion:  lee el archivo del  la carpeta y lo guarda en la base de datos y elimina
     * el archivo de la carpeta
     * parametros: null
     * return: el arreglo del estado del archivo de la carpeta
     * @return \Illuminate\Http\Response
     */ 
    public function importFacturas()
    {
        $request = request();
        $id_usuario = Auth::user()->id;
        $carpeta = 'facturas/'.$id_usuario.'/';
        // $carpeta = 'facturas';

        $pdfs = File::files(public_path().'/'.$carpeta);
        if (empty($pdfs)) {
            $msg=Factura::select('numero','razon_social','cufe','estado_envio')->take(5)
            ->orderBy('id' , 'desc')
            ->get();
            return redirect()->back()->with(compact('msg'));
        }
        
        $resultado = [];
        foreach ($pdfs as $pdf) {
            $filename = $carpeta.'/'.$pdf->getFilename();

            $parseador = new \Smalot\PdfParser\Parser();
            $documento = $parseador->parseFile($filename);
            
            $paginas=$documento->getPages();
            // dd($paginas);
            $size=sizeof($paginas);
            $i=0;
            foreach ($paginas as $page) {
                $texto=$page->getText();
                $seccionespdf= $this->pdfToArray($texto);
                $totales=explode("\n",$seccionespdf[8]);
                //porcentaje Descuento
                $tott1=explode("|",$totales[1]);
                $pdesc=trim($tott1[3]);
                $por_desc=substr($pdesc,12);
                $porc=$porc_desc=explode("%",$por_desc);
                $porc_descu=$porc[0];
                //Descuento
                $tott2=explode("|",$totales[2]);
                $desc=trim($tott2[3]);
                $nomdescu=['porc_descu','descuento'];
                $valdesc=[$porc_descu,$desc];
                $descuento = array_combine($nomdescu, $valdesc);
                // dd($descuento);
                $impuestos=explode("\n",$seccionespdf[9]);
                $imp=explode("|",$impuestos[2]); 
                $nomimpuestos=['rte_fuente', 'rte_iva','rte_ica'];
                $rte_fuente=trim($imp[1]); 
                $rte_iva=trim($imp[2]);
                $rte_ica=trim($imp[3]);
                $impu=[$rte_fuente,$rte_iva,$rte_ica];
                $retenciones = array_combine($nomimpuestos, $impu);

                $fec=explode("|",$seccionespdf[1]);
                $fech=explode(":",$fec[3]);
                $fecha_emision=$fech[1];

                $venc=explode("\n",$seccionespdf[3]);
                $feve=explode("|",$venc[2]);
                $fecvenci=trim($feve[3]);
                $forpag=trim($feve[2]);
                $nominfo=['fecha_emision','fecha_vencimiento','forma_pago'];
                $valinfo=[$fecha_emision,$fecvenci,$forpag];
                $info=array_combine($nominfo,$valinfo);
                $otro=$seccionespdf[5];
                
                $infotro=explode("\n",$otro);
                $ifotro=explode("|",$infotro[2]);

                $otronum=[trim($ifotro[2]),trim($ifotro[3]),trim($ifotro[4]),trim($ifotro[5]),trim($ifotro[6])];
                $ref=$otronum;
                $numeracion=trim(end($seccionespdf));
                $numer=explode("|",$numeracion);

                $string = preg_replace("/[\n]+/", " ", $numer);
                if(trim($string[1]) =='FF'){
                    unset($string[1]);
                } 
                sort($string);
                // dd($string);
                unset($string[0],$string[1],$string[2],$string[3],$string[4],$string[5],$string[6],$string[7],$string[8]);
                sort($string);
                // dd($string);
                if (sizeOf($string) == 8) {
                    unset($string[0],$string[1],$string[2],$string[3]);
                }
                if (sizeOf($string) == 6) {
                    unset($string[0],$string[1]);
                }
                if (sizeOf($string) == 7) {
                    unset($string[0],$string[1],$string[2]);
                }
                sort($string);
                $cabeceras_autorizacion=['num_autorizacion','prefijo'];
                $num_autorizacion=array(substr($string[3], 17,16));
                
                $prefijo=array(substr($string[3], 63,5));
                
                $cabeceras_autorizacion=['num_autorizacion','prefijo'];
                $autorizacion=array_merge($num_autorizacion, $prefijo);
            
                if(!empty($autorizacion)){
                    $autorizacion2 = array_combine($cabeceras_autorizacion, $autorizacion);
                }
                //  dd($autorizacion2['num_autorizacion']);
                $propiedades=['numero','cliente','contacto','nit o c.c','direccion','ciudad','telefono',
                'email','rte fuente','impoconsumo','rte iva','rte ica','rte cree','Vendedor',
                'forma de pago','Fecha de Expiracion','Cod Ciudad','Zona'];
                // dd($propiedades);
                $atributos = $this->readFromPdf($propiedades,$seccionespdf);
            
                // dd($atributos);
                $productos= $this->getproductos($seccionespdf);
                // dd($productos);
                
                // dd($productos);$atributos,$productos,$nombrearchivo,$autorizaciones,$ref,$info,$retenciones,$porcendesc,$desc
                $verificacion= $this->storeFromFile($atributos, $productos, $filename, $autorizacion2,$ref,$info,$retenciones,$descuento,$id_usuario);
                
                if($descuento['descuento'] != '0.00'){
                    // dd($descuento['descuento']);
                    $descu_global= $this->getdescuentoglob($descuento,$atributos,$autorizacion2);
                }
                // dd($verificacion);
            }
        }
        $facturasvali=FacturaValidar::where('id_usuario','=',$id_usuario)->get();
        // dd($facturasvali);  
        
        foreach($facturasvali as $facturavali) {
            // dd($facturavali);
            $factur=Factura::where('numero','=',$facturavali->numero)->first();
            // dd($factur);
            if (empty($factur)) {
                //  dd($factur);
                $id='';
                $Store_fact= $this->StoreFacturas($facturavali,$id);
                // dd($Store_fact);
                $eliminarfact=FacturaValidar::find($facturavali->id);
                $eliminarfact->detalles()->delete();
                $eliminarfact->delete();
            }else{
                if ($factur->estado == 1) {
                    $deletefact=FacturaValidar::find($facturavali->id);
                    $deletefact->detalles()->delete();
                    $deletefact->delete();
                }else{
                    $id=$factur->id;
                    $eliminafact=Factura::find($factur->id);
                    $eliminafact->detalles()->delete();

                    $Store_fact= $this->StoreFacturas($facturavali,$id);

                    $elimifact=FacturaValidar::find($facturavali->id);
                    $elimifact->detalles()->delete();
                    $elimifact->delete();
                }
            }
        } 
        // dd($facturasvali);  
        // if ($verificacion) {
            $eliminacion = File::delete($filename);
            if ($eliminacion) {
                $aut_numera=trim( $autorizacion2['num_autorizacion']);
                $prefijo=trim( $autorizacion2['prefijo']);
                $numeracion = PrefijoNumeracion::where('autorizacion','=',$aut_numera)->where('prefijo','=',$prefijo)->first();
                $folio=intval(str_replace('-','',(substr($atributos['numero'],$numeracion['rango_prefijo']))));
                $prefijo=substr($atributos['numero'],0,$numeracion->rango_prefijo);
                $numero=$prefijo.$folio;
                $resultado[] = "archivo ".$filename.$numero." Guardado. eliminacion satisfatoria";
                return $resultado;
                // return redirect()->action("SoapController@Transmitir", [
                //     'numero'=> $numero,
                //     'dto' =>'invoic',
                // ]);
            }else {
            
                $resultado[] = "archivo ".$filename." guardado. eliminacion fallida";
                
        }  
         
        $factu=Factura::get();
        // dd($factu);

    }
    private function multiexplode ($delimiters,$data) {
        $MakeReady = str_replace($delimiters, $delimiters[0], $data);
        $Return    = explode($delimiters[0], $MakeReady);
        return  $Return;
    }
    /**
     * DESCRIPCION: toma el archivo de la carpeta lo divide por secciones separadas
     * por + y elimina las posiciones que solo tiene -- y vacias
     * PARAMETROS: el nombre del archivo
     * VALORES DE RETORNO: el array del archivo ya separa por secciones y limpio
     */
    private function pdfToArray($texto){
        
        $seccionespdf = $this->multiexplode(array("+-","-+"),$texto);
        // dd($seccionespdf);
        // $seccionespdf = explode('-+', $texto);
        // dd($seccionespdf[17]);
        $array1=trim($seccionespdf[0]);
        $array2=trim($seccionespdf[6]);
        // dd($array1);
        $valor1= "DOSPrinter 3.4 DEMO";
        $valor2="surtiplazachecho@hotmail.com";
        if ($array1 == $valor1) {
            unset($seccionespdf[0]);
            // $secciones1pdf = asort($seccionespdf);
        }
        if ($array2 == $valor2) {
            unset($seccionespdf[6]);
            // $secciones1pdf = asort($seccionespdf);
        }
        // sort($seccionespdf);
        $seccionespdf = array_values(array_filter($seccionespdf, function($seccion){
            $pos = strpos($seccion,"--");
            $pos1 = strpos($seccion,"--");
            return !(($pos !== false)  && ($pos1 == false) || (empty(trim($seccion))));
            // return !(($pos1 == false) || (empty(trim($seccion)))) ;
        }));
        // dd($seccionespdf);
        // var_dump($seccionespdf);
        return $seccionespdf;
    }
    /**
     * DESCRIPCION: toma el arreglo de la propiedades que necesito y el pdf separado por +
     * y haciendo uso de la funcion getproperty retorna un arreglo con todas las propiedades solicitadas
     * PARAMETROS: las propiedades y las secciones separadas por +
     * VALORES DE RETORNO: arreglo con los valores de las propiedades
     */
    private function getdescuentoglob($descuento,$atributos,$autorizaciones){
        // dd($descuento,$atributos['numero']);
        $aut_numera=trim($autorizaciones['num_autorizacion']);
        $prefijo=trim($autorizaciones['prefijo']);
        $descuent=str_replace(',','',$descuento['descuento']);
        $numeracion = PrefijoNumeracion::where('autorizacion','=',$aut_numera)->where('prefijo','=',$prefijo)->first();
            // dd($numeracion);
            $folio=intval(str_replace('-','',(substr($atributos['numero'],$numeracion['rango_prefijo']))));
            $prefijo=substr($atributos['numero'],0,$numeracion->rango_prefijo);
            // dd();
            $numero=$prefijo.$folio;
        $fact=Factura::where('numero','=',$numero)->first();
            // dd($fact);
            $descuitem=0;
            $total_bruto=0;
            $detalles=DetalleFactura::where('id_factura','=',$fact->id)->get();
            foreach ($detalles as $detalle) {
                // dd($detalle->descuento);
               $descuitem+=$detalle->descuento;
               $total_bruto+=$detalle['cantidad']*$detalle['precio_unit'];     
            }
            // dd($descuitem);
            if($descuitem == 0.0 || $descuitem == 0.00 || $descuitem == 0){
                // $tari_desc=round(($descuento*100/$total_bruto),1);
                // $descuentoxitem=$tari_desc; 
                foreach ($detalles as $detalle) {
                    $tot_item=$detalle['cantidad']*$detalle['precio_unit'];
                    $val_desc=$tot_item*$descuento['porc_descu']/100;
                    $detalles=DetalleFactura::find($detalle->id);
                    $detalles->porcentaje_descu =$descuento['porc_descu'];
                    $detalles->descuento = $val_desc;
                    $detalles->save();
                }
            }else{
                $detalles=DetalleFactura::where('id_factura','=',$fact->id)->get()->last();
                // dd($detalles['porcentaje_descu']);
                $id=$detalles->id;                
               $total=$detalles['valor_total'];
                // dd($total);
                $i=0;
                if($total > $descuent && $detalles['porcentaje_descu'] == '0.00'){
                    $tari_descu=($descuent*100)/$total;
                    $detalles=DetalleFactura::find($id);
                    $detalles->porcentaje_descu =$tari_descu;
                    $detalles->descuento = $descuent;
                    $detalles->save();
                }else{
                    $detalles=DetalleFactura::where('id_factura','=',$fact->id)->get();
                    foreach ($detalles as $detal) {
                        // dd($detal);
                        $i++;
                        $ids=$id; 
                        $id_it=$ids-$i;
                        $detalle=DetalleFactura::where('id','=',$id_it)->first();
                        $tota=$detalle['valor_total'];
                        $porcen_desc=intval($detalle->porcentaje_descu);
                        if(intval($tota) > intval($descuent) && $porcen_desc == 0){
                            $tari_descu=($descuent*100)/$tota;
                            $detall=DetalleFactura::find($id_it);
                            $detall->porcentaje_descu =$tari_descu;
                            $detall->descuento = $descuent;
                            $detall->observacion='Descuento global';
                            $detall->save();
                        break;  
                        }
                }
                
            }
        }
       return $descuento;
        
    }

    private function readFromPdf($p_propiedades, $p_seccionespdf){
        $resultado=[];
        foreach ($p_seccionespdf as $seccionpdf) {
            $seccionpdf = explode('|', $seccionpdf);
            // dd($seccionpdf);
            foreach ($p_propiedades as $propiedad) {
                $consulta = $this->getproperty($seccionpdf,$propiedad);
                // dd($p_propiedades);
                // if($propiedad == 'fecha de vencimiento'){
                //     $propiedad=;
                // }
                if (!empty($consulta)) {
                    $resultado[$propiedad] = $consulta;

                }
            }
        }
        // dd($resultado);
        return $resultado;
    }
    /**
     * descripcion: guarda los datos de la factura
     * con todos los detalles de los productos y el nombre de el archivo guardado
     * parametros: recibe 3 parametros el primero es la informacion de la factura,
     * el segundo los detalles de los productos y el trecero el nombre del archivo
     * return: true cuando se guardo toda la informacion o false cuando hubo algun error
     */
    private function storeFromFile($atributos,$productos,$nombrearchivo,$autorizaciones,$ref,$info,$retenciones,$descuento,$id_usuario){
        DB::beginTransaction();
        try{

            // dd($atributos,$productos,$nombrearchivo,$autorizaciones,$ref,$info,$retenciones,$descuento);
            // dd($retenciones);
            $aut_numera=trim($autorizaciones['num_autorizacion']);
            $prefijo=trim($autorizaciones['prefijo']);
            // dd($aut_numera);
            $numeracion = PrefijoNumeracion::where('autorizacion','=',$aut_numera)->where('prefijo','=',$prefijo)->first();
            // dd($numeracion);
            $folio=intval(str_replace('-','',(substr($atributos['numero'],$numeracion['rango_prefijo']))));
            $prefijo=substr($atributos['numero'],0,$numeracion->rango_prefijo);
            // dd();
            $numero=$prefijo.$folio;
            // dd($numero);
            // $fact= Factura::where('numero','=',$numero)->first();
            $fact= Factura::where('numero','=',$numero)->first();

            $ultimo = DB::table('facturas')
            ->max('folio');
                // dd($ultimo);
                if(empty($ultimo)){
                    $ultnum=0;
                }else{
                $ultnum=$ultimo;
                
                }  
            $ultpre=$numeracion->prefijo;
            // dd($ultpre);
            $proxnum=$ultnum+1;
            $prox=$ultpre.$proxnum; 
        $fact= FacturaValidar::where('numero','=',$numero)->first();
        
            if(empty($fact)){
                
                
                    $nume_autori=trim($autorizaciones['num_autorizacion']);
                    // dd($nume_autori);
                    $numeracion = PrefijoNumeracion::where('autorizacion','=',$nume_autori)->where('prefijo','=',$prefijo)->where('estado','=', 0)->first();
                    //  dd($numeracion);
                    if (!empty($numeracion)) {
                        exit("Numeracion Autorizaci贸n ($nume_autori) no esta Activa");
                    }else{
                    }
                    if (array_key_exists('email', $atributos)) {
                        $email =$atributos['email'];
                       }else{
                           $email='surtiplazachecho@hotmail.com';
                       }
                       $email =$email;
                    //  $pos = strpos($email, '@');
                    if (false !== strpos($email, "@") && false !== strpos($email, ".")) {
                         $email2 =$email;
                    } else {
                        $email2 ='surtiplazachecho@hotmail.com';
                    }
                    // dd($email2);
                    
                    $numeracion_au= PrefijoNumeracion::select('prefijos_numeracion.*')
                    ->where('autorizacion','=',$aut_numera)->where('prefijo','=',$prefijo)->first();
                    // dd($numeracion_au);
                    $factura= new FacturaValidar;
                    $factura->num_autorizacion=$numeracion_au->autorizacion;
                    $factura->id_usuario=$id_usuario;
                    $factura->fec_ini_autorizacion=$numeracion_au->fecha_inicio;
                    $factura->fec_fin_autorizacion=$numeracion_au->fecha_fin;
                    $factura->prefijo_rango_nume=$numeracion_au->prefijo;
                    $factura->rango_prefijo=$numeracion_au->rango_prefijo;
                    $factura->vigencia=$numeracion_au->vigencia;
                    $factura->rango_numeracion_min=utf8_encode($numeracion_au->numero_inicia);
                    $factura->rango_numeracion_max=utf8_encode($numeracion_au->numero_final);
                    $factura->observacion=$numeracion_au->tipo_documento;
                    $nume=$atributos['numero'];
                    $rango=$numeracion_au->rango_prefijo;
                    // $folio=substr($nume,$rango);
                    // dd($folio);
                    $factura->folio = $folio;
                

                
               
                $factura->numero = $numero;
                $fech=$info['fecha_emision'];
                $factura->fecha = $fech;
                
                @$factura->fecha_vencimiento = trim($info['fecha_vencimiento']);
                $factura->oc_nro=trim($ref[0]);
                $factura->ov_nro=trim($ref[1]);
                $factura->dcto_alt=trim($ref[2]);
                $factura->remisiones=trim($ref[3]);
                $factura->hora = date('H:i:s');
                if (array_key_exists('vendedor', $atributos)) {
                   $factura->vendedor = utf8_encode($atributos['vendedor']);
               }else{
                  $factura->vendedor = '';
               }
                
                @$factura->rte_fuente = str_replace(',','',$retenciones['rte_fuente']);
                @$factura->impoconsumo = 0.00;
                @$factura->rte_iva = str_replace(',','',$retenciones['rte_iva']);
                @$factura->rte_ica = str_replace(',','',$retenciones['rte_ica']);
                @$factura->descuento = str_replace(',','',$descuento['descuento']);
                @$factura->porc_desc = str_replace('%','',$descuento['porc_descu']);
                $nit_adquiriente=explode(" ",$atributos['nit o c.c']);
                $identifi_adqui =$nit_adquiriente[0];
                $ejemplo = explode("-", $nit_adquiriente[0]);
                $cant=strlen($ejemplo[0]);
                $p_digito=substr($ejemplo[0],0,1);
                // dd($p_digito);
                if ($p_digito == 9 || $p_digito == 8 && strlen($ejemplo[0]) == 9 ) {
                    $tip_identifi=31;
                    $ident_tip_pers=1;
                    $regimen=48;
                    $razon_social=utf8_encode($atributos['cliente']);
                    $nombre_adqui=utf8_encode($atributos['cliente']);
                    $apellido_adqui=' ';
                }else{
                    $tip_identifi=13;
                    $ident_tip_pers=2;
                    $regimen=49;
                    $razon_social=utf8_encode($atributos['cliente']);
                    $cant1 = count(explode(" ", $razon_social));
                    $nombre_adqui=utf8_encode($atributos['cliente']);
                    $apellido_adqui=' ';
                }

                $adqui = Adquiriente::where('identifi_adqui','=',$nit_adquiriente[0])->first();
                if(empty($adqui)){
                    $adquiriente=new Adquiriente;
                    $adquiriente->ident_tip_pers= $ident_tip_pers;
                    $adquiriente->identifi_adqui =$nit_adquiriente[0];
                    $adquiriente->tip_identifi =$tip_identifi;
                    $adquiriente->regimen ='49';
                    $adquiriente->razon_social = $razon_social;
                    $adquiriente->nombre_adqui = $nombre_adqui;
                    $adquiriente->apellidos_adqui = $apellido_adqui;
                    $adquiriente->email =$email2;
                    $adquiriente->direccion = $atributos['direccion'];
                    $adquiriente->nom_regis_rut = $razon_social;
                    $municipio=$atributos['ciudad'];
                    if($atributos['ciudad'] == 'OCA#A'){
                        $municipio='OCANA';
                    }else{
                        $municipio=$atributos['ciudad'];
                    }
                    $inf_municipio= Municipio::select('municipios.*')->where('nombre_municipio','=',$municipio)->first();
                    if(empty($inf_municipio)){
                        @$id_municipio=1007;
                        $cod_postal=7676001;
                    }else{
                        @$id_municipio=$inf_municipio->id;
                        $cod_postal=$inf_municipio->codigo_departamento.$inf_municipio->codigo_municipio;
                    }
                    @$adquiriente->id_municipio = $id_municipio;
                    $adquiriente->cod_postal = $cod_postal;
                    $adquiriente->responsa_adqui = 'R-99-PN';
                    if(!isset($atributos['telefono'])){
                        $telefono=0;
                    }else{
                        $telefono=$atributos['telefono'];
                    }
                    $adquiriente->telefono = $telefono;
                    $adquiriente->ident_tributo = '01';
                    $adquiriente->nom_tributo = 'IVA';
                    $adquiriente->save();

                }else{
                    $adq=Adquiriente::findorfail($adqui->id);
                    $municipio=$atributos['ciudad'];
                    if($atributos['ciudad'] == 'OCA#A'){
                        $municipio='OCANA';
                    }else{
                        $municipio=$atributos['ciudad'];
                    }
                    $inf_municipio= Municipio::select('municipios.*')->where('nombre_municipio','=',$municipio)->first();
                    if(empty($inf_municipio)){
                        @$id_municipio=1007;
                        $cod_postal=7676001;
                    }else{
                        @$id_municipio=$inf_municipio->id;
                        $cod_postal=$inf_municipio->codigo_departamento.$inf_municipio->codigo_municipio;
                    }
                    @$id_municipio=$id_municipio;
                    @$adq->id_municipio = $id_municipio;
                    // $adq->email =$atributos->email;
                    $adq->save();
                    
                }
                if(!empty($atributos['contacto'])){
                    $contac= Contacto::where('nombre_contacto','=',$atributos['contacto'])->first();
                    if(empty($contac)){
                        if(!isset($atributos['telefono'])){
                            $telefono=0;
                        }else{
                            $telefono=$atributos['telefono'];
                        }
                        $contacto = new Contacto;
                        $contacto->id_adquiriente=0;
                        $contacto->tipo_contacto= 1;
                        $contacto->nombre_contacto=$atributos['contacto'];
                        $contacto->cargo_contacto='Persona de contacto';
                        $contacto->telefono_contacto=$telefono;
                        $contacto->email =$email2;
                        $contacto->save();
                    }
                }
                if (!empty($atributos['contacto'])) {
                    $conta= Contacto::where('nombre_contacto','=',$atributos['contacto'])->first();
                    if (!empty($conta)) {
                        $factura->tipo_contacto= $conta->tipo_contacto;
                        $factura->nombre_contacto=$conta->nombre_contacto;
                        $factura->cargo_contacto=$conta->cargo_contacto;
                        $factura->telefono_contacto=$conta->telefono_contacto;
                        $factura->email_contacto=$conta->email_contacto;
                    }else {
                    }
                }else {
                    if(!isset($atributos['telefono'])){
                        $telefono=0;
                    }else{
                        $telefono=$atributos['telefono'];
                    }
                    $factura->tipo_contacto= 1;
                    $nombre_contacto=utf8_encode($atributos['cliente']);
                    $factura->nombre_contacto=$nombre_contacto;
                    $factura->cargo_contacto='Persona de contacto';
                    $factura->telefono_contacto=$telefono;
                    $factura->email_contacto=$email2;
                }
                $dat_adquiri = Adquiriente::where('identifi_adqui','=',$nit_adquiriente[0])->first();
                @$factura->ident_tip_pers = $dat_adquiri->ident_tip_pers;
                @$factura->identifi_adqui = $dat_adquiri->identifi_adqui;
                $factura->tip_identifi = $dat_adquiri->tip_identifi;
                $factura->regimen = $dat_adquiri->regimen;
                $factura->razon_social = $razon_social;
                $factura->nombre_adqui = $nombre_adqui;
                $factura->apellidos_adqui = $apellido_adqui;
                $factura->direccion = $atributos['direccion'];
                $factura->nom_regis_rut = $razon_social;
                
                $municipio=explode(" Cod Ciudad:",$atributos['ciudad']);
                // dd($municipio);
                $inf_municipio= Municipio::select('municipios.*')->where('nombre_municipio','=',trim($municipio[0]))->first();
                @$id_municipio=$inf_municipio->id;
                // dd($id_municipio);
                $cod_postal=$inf_municipio->codigo_departamento.$inf_municipio->codigo_municipio;
                
                if(!isset($atributos['telefono'])){
                    $telefono=0;
                }else{
                    $telefono=$atributos['telefono'];
                }
                $factura->id_municipio = $id_municipio;
                $factura->responsa_adqui = $dat_adquiri->responsa_adqui;
                $factura->telefono = $telefono;
                $factura->email_contacto =$email2;
                $factura->ident_tributo = '01';
                $factura->nom_tributo = $dat_adquiri->nom_tributo;
                $factura->cod_postal = $cod_postal;
                $factura->estado=0;
                
                if (array_key_exists('forma de pago', $atributos)) {
                    $forma_pago=$atributos['forma de pago'];
                }else{
                    $forma_pago=trim($info['forma_pago']);
                }
                if($forma_pago == 'contado' || $forma_pago == 'Contado' || $forma_pago == 'CONTADO'){
                    $factura->cod_med_pago='ZZZ';
                    $factura->metodo_pago=2;
                }elseif($forma_pago == 'credito' || $forma_pago == 'Credito' || $forma_pago == 'CREDITO'){
                    $factura->cod_med_pago=1;
                    $factura->metodo_pago=1;
                }
                $factura->forma_pago=$forma_pago;

                @$factura->save();
                
                $id_factura=$factura->id;
                // $met_pago=explode(" ",$forma_pago);
                
                // if (array_key_exists('1', $met_pago)) {
                //    $dias=intval($met_pago[1]);
                //    }else{
                //        $dias=1;
                //    }
                
                // if ($dias >= 8) {
                //     $fact = Factura::find($id_factura);
                //     $fact->cod_med_pago='ZZZ';
                //     $fact->metodo_pago=2;
                //     $fact->save();
                // } else{
                //     $fact = Factura::find($id_factura);
                //     $fact->cod_med_pago=1;
                //     $fact->metodo_pago=1;
                //     $fact->save();
                // }
                
                    foreach ($productos as $producto) {
                        $detal= Producto::where('referencia','=',$producto['referencia'])->first();
                        if (empty($detal)) {
                            // if($producto['referencia'] != '005313'){
                            $detalles = new Producto;
                            $detalles->referencia = $producto['referencia'];
                            $detalles->descripcion =utf8_encode($producto['descripcion']);
                            $detalles->um =$producto['um'];
                            $detalles->precio_unit =intval(str_replace(',','',$producto['precio_unit']));
                            $detalles->descuento =(str_replace('%','',$producto['descuento']));
                            if (array_key_exists('impuesto', $producto)) {
                                $detalles->impuesto =$producto['impuesto'];
                           }else{
                                $detalles->impuesto =0.00;
                           }
                            $detalles->save();
                        // }
                        }else{}

                    }
                // dd($detalle);
                foreach ($productos as $producto) {
                    // dd($producto);
                    $cantidad=(str_replace(',','',$producto['cantidad']));
                    $precio_unit=(str_replace(',','',$producto['precio_unit']));
                    $total_item = $cantidad*$precio_unit;
                    $detalle = new DetalleFactValidar;
                    $detalle->id_factura = $factura->id;
                    $detalle->referencia = $producto['referencia'];
                    $detalle->descripcion =utf8_encode($producto['descripcion']);
                    $detalle->mo = 1 ;
                    $detalle->cantidad =$cantidad;
                    if ($producto['um'] == 'UND') {
                       $um=94;
                    }elseif($producto['um'] == 'KIL'){
                        $um='KGM';
                    }else{
                        $um=$producto['um'];
                    }
                    $detalle->um =$um;
                    $detalle->precio_unit =$precio_unit;
                    $pordentaje_descu=(str_replace('%','',$producto['descuento']));
                    $val_descuento=$total_item*$pordentaje_descu/100;
                    $detalle->porcentaje_descu =$pordentaje_descu;
                    $detalle->descuento = $val_descuento;
                    $detalle->impoconsumo = intval(str_replace(',','',$producto['inc']));
                    $detalle->valor_total =intval(str_replace(',','',$producto['valor_total']));
                    if (array_key_exists('iva', $producto)) {
                        $detalle->impuesto =$producto['iva'];
                   }else{
                        $detalle->impuesto =0.00;
                   }
                    $detalle->save();
                
                }
        }else{
            //  dd($retenciones);
            $fat=Factura::find($fact->id);
            // dd($fat);
            @$fat->rte_fuente = str_replace(',','',$retenciones['rte_fuente']);
            @$fat->impoconsumo = 0.00;
            @$fat->rte_iva = str_replace(',','',$retenciones['rte_iva']);
            @$fat->rte_ica = str_replace(',','',$retenciones['rte_ica']);
            @$fat->descuento = str_replace(',','',$descuento['descuento']);
            @$fat->porc_desc = str_replace('%','',$descuento['porc_descu']);
            @$fat->save();
            
            foreach ($productos as $producto) {
                
                // if($producto['referencia'] != '005313'){
                // if($producto['referencia'] != '024800'){
                    $cantidad=(str_replace(',','',$producto['cantidad']));
                    $precio_unit=(str_replace(',','',$producto['precio_unit']));
                    $total_item = intval(str_replace(',','',$producto['valor_total']));
                    $detalle = new DetalleFactValidar;
                    $detalle->id_factura = $fact->id;
                    $detalle->referencia = $producto['referencia'];
                    $detalle->descripcion =utf8_encode($producto['descripcion']);
                    $detalle->mo = 1 ;
                    $detalle->cantidad =$cantidad;
                    if ($producto['um'] == 'UND') {
                       $um=94;
                    }elseif($producto['um'] == 'KIL'){
                        $um='KGM';
                    }else{
                        $um=$producto['um'];
                    }
                    $detalle->um =$um;
                    $detalle->precio_unit =$precio_unit;
                    $pordentaje_descu=(str_replace('%','',$producto['descuento']));
                    $val_descuento=0;
                    $detalle->porcentaje_descu =$pordentaje_descu;
                    $detalle->descuento =$pordentaje_descu;
                    // $detalle->impoconsumo = $producto['inc'];
                    $detalle->impoconsumo = intval(str_replace(',','',$producto['inc']));
                    $detalle->valor_total =intval(str_replace(',','',$producto['valor_total']));
                    if (array_key_exists('iva', $producto)) {
                        $detalle->impuesto =$producto['iva'];
                   }else{
                        $detalle->impuesto =0.00;
                   }
                    $detalle->save();
                 
            }
                $log = new LogImportacion;
                $log->id_factura=$fact->id;
                $log->filename=$nombrearchivo;
                $log->save();

                // $fact->id;
        } 
           DB::commit();
            return true;
        }catch(\Excepcion $e){
            return false;
        }
    }
    /**
     * descripcion:esta funcion recibe como parametro el pdf separados por | y la propiedad que quiero
     * y retorna el valor de la propiedad que pedi
     * parametros: pdf=array | ,propiedad=string
     * return: matriz de dos dimensiones de valores
     */
    private function getproperty($pdf,$propiedad){
        $resultado = [];
        if (is_array($pdf)) {
            foreach ($pdf as $lineapdf)
            {
                $valorlinea = explode(":", $lineapdf,2);
                // dd($valorlinea);
                $tamanolinea = sizeof($valorlinea);
                if ($tamanolinea > 1 ) {

                    $pos = strpos(strtolower($valorlinea[0]), strtolower($propiedad));
                    if ($pos !== false) {
                        $resultado = trim($valorlinea[1]);
                    }
                }
            }
        }
        // dd($resultado);
        return $resultado;
    }
    /**
     * descripcion: me trae todos los detalles de todos los productos
     * parametros: todas secciones separadas por +
     * return: matriz de los productos
     */
    private function getproductos($seccionespdf)
    {
        // dd($seccionespdf);
        $resultado = [];
        foreach ($seccionespdf as $index => $seccionpdf) {
            // dd($seccionpdf);
            $pos = strpos($seccionpdf,":");
            if ($pos === false) {
                $listadoproductos= explode("|",$seccionespdf[$index+1]);
            //  dd($listadoproductos);
                $listadoproductos = array_filter($listadoproductos, function($linea){
                    return !empty(trim($linea));
                });
                
                $cabeceras = ['referencia', 'descripcion','cantidad','um', 'precio_unit', 'descuento', 'precunit_desc','iva','inc','valor_total'];
                $attr_cabecera = 0;
                
                foreach ($listadoproductos as $listadoproducto) {
                
                    $temp_cabecera= $cabeceras;
                    $listadoproducto = explode('  ',$listadoproducto);
                    $listadoproducto = array_filter($listadoproducto, function($linea){
                        return !empty(trim($linea));
                    });
                    // dd($listadoproducto);
                    $cantidad_atributos =sizeof($listadoproducto);
                    $listadoproducto = array_filter($listadoproducto, function($linea) use($cantidad_atributos){
                        return $cantidad_atributos>1;
                    });

                    $trimed= array_walk($listadoproducto,function (&$string, $index){
                        $string = trim($string);
                    });
                    $listadoproducto =array_values($listadoproducto);
                    
                    if (sizeOf($listadoproducto) == 8) {
                        unset($temp_cabecera[7]);
                    }
                    if (sizeOf($listadoproducto) == 9) {
                        $listadoproducto=[$listadoproducto[0],$listadoproducto[1],$listadoproducto[2],
                        $listadoproducto[3],$listadoproducto[4],$listadoproducto[5],$listadoproducto[6],$listadoproducto[7],'0',$listadoproducto[8]];
                    }
                    if (sizeOf($listadoproducto) == 11) {
                        $listadoproducto=[$listadoproducto[0],$listadoproducto[1].$listadoproducto[2],
                        $listadoproducto[3],$listadoproducto[4],$listadoproducto[5],$listadoproducto[6],$listadoproducto[7],$listadoproducto[8],$listadoproducto[9],$listadoproducto[10]];
                    }
                    if (sizeOf($listadoproducto) == 2) {
                        $listadoproducto=[];
                    }
                    if (sizeOf($listadoproducto) == 5) {
                        $listadoproducto=[];
                    }
                    // dd($listadoproducto);
                    // dd($listadoproducto,$temp_cabecera);
                    if(!empty($listadoproducto)){
                        $resultado[] = array_combine($temp_cabecera, $listadoproducto);
                    }
                }
            }
        }
        // dd($resultado);
        return $resultado;
    }
    private function StoreFacturas($facturavali,$id){
        // dd($facturavali,$id);
        // DB::beginTransaction();       
        // try{
        if(!empty($id)){
            $factura= Factura::find($id);    
        }else{
            $factura= new Factura;
        }
        // dd($factura);
        $factura->num_autorizacion=$facturavali->num_autorizacion;
        $factura->fec_ini_autorizacion=$facturavali->fec_ini_autorizacion;
        $factura->fec_fin_autorizacion=$facturavali->fec_fin_autorizacion;
        $factura->prefijo_rango_nume=$facturavali->prefijo_rango_nume;
        $factura->rango_prefijo=$facturavali->rango_prefijo;
        $factura->vigencia=$facturavali->vigencia;
        $factura->rango_numeracion_min=$facturavali->rango_numeracion_min;
        $factura->rango_numeracion_max=$facturavali->rango_numeracion_max;
        $factura->observacion=$facturavali->observacion;
        $factura->folio = $facturavali->folio;
        $factura->numero = $facturavali->numero;
        $factura->fecha = $facturavali->fecha;
        $factura->fecha_vencimiento = $facturavali->fecha_vencimiento;
        $factura->oc_nro=$facturavali->oc_nro;
        $factura->ov_nro=$facturavali->ov_nro;
        $factura->dcto_alt=$facturavali->dcto_alt;
        $factura->remisiones=$facturavali->remisiones;
        $factura->hora = $facturavali->hora;
        $factura->vendedor = $facturavali->vendedor;
        $factura->rte_fuente = $facturavali->rte_fuente;
        $factura->impoconsumo = 0.00;
        $factura->rte_iva = $facturavali->rte_iva;
        $factura->rte_ica =$facturavali->rte_ica;
        $factura->descuento = $facturavali->descuento;
        $factura->porc_desc = $facturavali->porc_desc;
        $factura->tipo_contacto= $facturavali->tipo_contacto;
        $factura->nombre_contacto=$facturavali->nombre_contacto;
        $factura->cargo_contacto=$facturavali->cargo_contacto;
        $factura->telefono_contacto=$facturavali->telefono_contacto;
        $factura->email_contacto=$facturavali->email_contacto;
        $factura->ident_tip_pers = $facturavali->ident_tip_pers;
        $factura->identifi_adqui = $facturavali->identifi_adqui;
        $factura->tip_identifi = $facturavali->tip_identifi;
        $factura->regimen = $facturavali->regimen;
        $factura->razon_social = $facturavali->razon_social;
        $factura->nombre_adqui = $facturavali->nombre_adqui;
        $factura->apellidos_adqui =$facturavali->apellido_adqui;
        $factura->direccion = $facturavali->direccion;
        $factura->nom_regis_rut = $facturavali->razon_social;
        $factura->id_municipio = $facturavali->id_municipio;
        $factura->responsa_adqui = $facturavali->responsa_adqui;
        $factura->telefono = $facturavali->telefono;
        $factura->email_contacto =$facturavali->email_contacto;
        $factura->ident_tributo = $facturavali->ident_tributo;
        $factura->nom_tributo = $facturavali->nom_tributo;
        $factura->cod_postal = $facturavali->cod_postal;
        $factura->estado=0;
        $factura->forma_pago=$facturavali->forma_pago;
        $factura->cod_med_pago=$facturavali->cod_med_pago;
        $factura->metodo_pago=$facturavali->metodo_pago;
        $factura->save();
        
        foreach ($facturavali->detalles as $producto) {
            $detalle = new DetalleFactura;
            $detalle->id_factura = $factura->id;
            $detalle->referencia = $producto->referencia;
            $detalle->descripcion =$producto->descripcion;
            $detalle->mo = $producto->mo;
            $detalle->cantidad =$producto->cantidad;
            $detalle->um =$producto->um;
            $detalle->precio_unit =$producto->precio_unit;
            $detalle->porcentaje_descu =$producto->porcentaje_descu;
            $detalle->descuento = $producto->descuento;
            $detalle->impoconsumo = $producto->impoconsumo;
            $detalle->valor_total =$producto->valor_total;
            $detalle->save();
        }   
        // return  $factura->id;    
    }
}



