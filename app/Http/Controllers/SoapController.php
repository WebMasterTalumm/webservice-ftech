<?php
namespace App\Http\Controllers;

use SoapClient;
use App\Factura;
use App\Paquete;
use App\Nota;
// use App\XmlNota;
use Illuminate\Http\Request;
use App\params\params;
use App\params\Xmlfact;
use App\params\XmlNota;
use App\params\XmlND;
use DOMDocument;

class SoapController extends BaseSoapController
{
    private $service;

    public function Metodos(){
        try {

            $pricesClass = new Params();
            $prices = $pricesClass->getparams();
            $wsdlUrl = $prices['wsdlUrl'];

            $client = new SoapClient($wsdlUrl);

            return($client->__getFunctions());
            // echo "<br><br>";
            // return($client->__getTypes());
            // return($client);
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
    public function Transmitir(){
        $request = request();
        // DD($request);
        $tokenOperacion=$request->tokenOperacion;
        $numero=$request->numero;
        $dto=$request->dto;
        // dd($numero,$dto);
        // $dto='invoic';
        // $numero='02FE103';
        $factura= Factura::select('facturas.*')->where('numero','=', $numero)->first();
        // dd($factura)
            if(!empty($factura)){
                $id=$factura->id;
                $folio=$factura->folio;
                $prefijo=$factura->prefijo_rango_nume;
                $tabla='$factura';
            }else{
                $nota= Nota::select('notas.*')->where('numero','=',$numero)->first();
                $id=$nota->id;
                $folio=$nota->folio;
                $prefijo=$nota->prefijo_rango_nume;
                $tabla='$nota';
            }
        // dd($folio,$id,$prefijo);
        
        // dd($tabla->transaccionID);
            if($dto == 'invoic'){
                $Classxmlfact = new Xmlfact();
                $xmlfact = $Classxmlfact->Xml();
                $xmlBase64    = $xmlfact['xmlBase64'];
                $numero = $xmlfact['numero'];
                // dd($numero);
            }elseif($request->dto == 'Nota'){
                $pricesClass = new XmlNota();
                // dd($pricesClass);
                $prices = $pricesClass->Xml();
                // DD($prices);
                $xmlBase64= $prices['xmlBase64'];
                $numero = $prices['numero'];
            }elseif($dto == 'nd'){
                $Classxmlnd = new XmlND();
                $xmlnd = $Classxmlnd->Xml();
                $xmlBase64    = $xmlnd['xmlBase64'];
                $numero = $xmlnd['numero'];
            }
        $params = new Params();
        $parametros = $params->getparams();
        $username    = $parametros['username'];
        $password   = $parametros['password'];
        $wsdlUrl = $parametros['wsdlUrl'];

       
        $uploadInvoiceFile=$this->FtechActionuploadInvoiceFile($username,$password,$wsdlUrl,$id,$xmlBase64,$dto);
        // dd($uploadInvoiceFile['code']);
        $code=$uploadInvoiceFile['code'];
        $success=$uploadInvoiceFile['success'];
        $error=$uploadInvoiceFile['error'];
        if($code != 201 ){
                $array = array(
                "code" => $code,
                "Mensaje"=>$success,
                "error"=> $error
            );
        return array($array);
        }
          $transaccionID=$uploadInvoiceFile['transaccionID'];

        $documentStatusFile= $this->FtechActiondocumentStatusFile($username,$password,$wsdlUrl,$id,$transaccionID,$dto);
        // DD($documentStatusFile);
            // $statusCode=$documentStatusFile->code;
            // $statuSuccess=$documentStatusFile->success;
        $downloadXML= $this->FtechActiondownloadXMLFile($username,$password,$wsdlUrl,$folio,$id,$prefijo,$dto);
            // $downlXMLCode=$downloadXML->code;  
            // $downlSuccess=$downloadXML->success;
        $getCUFEFile=$this->FtechActiongetCUFEFile($username,$password,$wsdlUrl,$folio,$id,$prefijo,$dto);
        // $cufe=$getCUFEFile->resourceData;

        // $store=$this->StoreResponse($id,$dto);
        // return $store;
        if($dto == 'invoic') {
            return redirect("facturas/importar");
        }elseif($dto == 'Nota'){
            return redirect("notas/importar");
        }

    }     
    public function FtechActionuploadInvoiceFile($username,$password,$wsdlUrl,$id,$xmlBase64,$dto){
        try{

            $params = array(
                "username" => $username,
                "password" => $password,
                "xmlBase64" => $xmlBase64,
            );
            // dd($params);
            $client = new SoapClient($wsdlUrl);
            $response = $client->__soapCall("FtechAction.uploadInvoiceFile", $params);
            // dd($response);
                
            if ($dto == 'invoic') {
                $factura = Factura::find($id);
                $factura->transaccionID= $response->transaccionID;
                $factura->save();
            }else{
                $nota = Nota::find($id);
                $nota->transaccionID= $response->transaccionID;
                $nota->save();
            }
            
                
            return [
                'code'=> $response->code,
                'success'=> $response->success,
                'transaccionID'=> $response->transaccionID,
                'error'=> $response->error,
        ];
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
    public function FtechActiondocumentStatusFile($username,$password,$wsdlUrl,$id,$transaccionID,$dto){
        try {
            $params = array(
                "username" => $username,
                "password" => $password,
                "transaccionID" => $transaccionID,
            );
            $client = new SoapClient($wsdlUrl);
            $response = $client->__soapCall("FtechAction.documentStatusFile", $params);
            if($response->code != 201 || $response->code != 200) 
                {
                    for ($i=0; $i < 2; $i++) { 
                    $client = new SoapClient($wsdlUrl);
                        $response = $client->__soapCall("FtechAction.documentStatusFile", $params);
                    }
                }
            // $factura= Factura::select('id')->where('numero','=',$numero)->first();
            // dd($factura);
            if ($dto == 'invoic') {
                $factura = Factura::find($id);
                $factura->estado_envio= $response->success;
                $factura->estado= 1;
                $factura->save();
            }else{
                $nota= Nota::find($id);
                $nota->estado_envio= $response->success;
                $nota->estado= 1;
                $nota->save();
            }
             
            return $response;
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
    public function FtechActiondownloadXMLFile($username,$password,$wsdlUrl,$folio,$id,$prefijo,$dto){
        try {
            
            $pricesClass = new Params();
            $prices = $pricesClass->getparams();
            $username    = $prices['username'];
            $password   = $prices['password'];
            $wsdlUrl = $prices['wsdlUrl'];

            $params = array(
                "username" => $username,
                "password" => $password,
                "prefijo" =>  $prefijo,
                "folio" => $folio,
          );
            $client = new SoapClient($wsdlUrl);
            $response = $client->__soapCall("FtechAction.downloadXMLFile", $params);
                if($response->code != 201 || $response->code != 200) 
                {
                    for ($i=0; $i < 2; $i++) { 
                    $client = new SoapClient($wsdlUrl);
                        $response = $client->__soapCall("FtechAction.downloadXMLFile", $params);
                    }
                }
            if ($dto == 'invoic') {
                $factura = Factura::find($id);
                $factura->observacion_envio= $response->success;
                $factura->save();
            }else{
                  $factura = Factura::find($id);
                $factura->observacion_envio= $response->success;
                $factura->save();
            }

            return $response;
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
    public function FtechActiongetCUFEFile($username,$password,$wsdlUrl,$folio,$id,$prefijo,$dto){
        try {

            $params = array(
                "username" => $username,
                "password" => $password,
                "prefijo" =>  $prefijo,
                "folio" => $folio,
            );
            $client = new SoapClient($wsdlUrl);
            $response = $client->__soapCall("FtechAction.getCUFEFile", $params);
                if($response->code != 201 || $response->code != 200) 
                {
                    for ($i=0; $i < 3; $i++) { 
                        $client = new SoapClient($wsdlUrl);
                        $response = $client->__soapCall("FtechAction.getCUFEFile", $params);

                        // if ($response->code == 201) {
                        //     break;
                        // }
                    }
                }
            // dd($response);
            if ($dto == 'invoic') {
                $factura = Factura::find($id);
                $factura->cufe= $response->resourceData;
                $factura->save();
            }else{
                $nota = Nota::find($id);
                $nota->cude= $response->resourceData;
                $nota->save();
            }
            
            return $response;
            
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
    public function StoreResponse($id,$dto){

        $factura=Factura::where('id','=',$id)->first();

        

    }

    public function FtechActiongetQRFile(){
        try {
            $request = request();
            $prefijo=$request->prefijo;
            $folio=$request->folio;
            $id=$request->id;
            $numero=$request->numero;
            $tokenOperacion=$request->tokenOperacion;

            $pricesClass = new Params();
            $prices = $pricesClass->getparams();
            $username    = $prices['username'];
            $password   = $prices['password'];
            $wsdlUrl = $prices['wsdlUrl'];


            $params = array(
                "username" => $username,
                "password" => $password,
                "prefijo" => $prefijo,
                "folio" => $folio,
          );

            $client = new SoapClient($wsdlUrl);
            $response = $client->__soapCall("FtechAction.getQRFile", $params);
            if ($response->code != 201) {
                return[
                    "code" => $response->code,
                    "Mensaje"=>$response->success,
                    "error"=> $response->error
                ];
            }
            $factura= Factura::select('id')->where('numero','=',$numero)->first();
             if (!empty($factura)) {
               
                return redirect("facturas/importar?numero=$factura");

                }else{
                    $editar_nota= Nota::find($id);
                    if ($editar_nota->tipo_documento == 91) {
                        $tipo_documento='Nota Credito';
                        }elseif ($editar_nota->tipo_documento == 92){
                            $tipo_documento='Nota Debito';
                        }
                        $numero=$editar_nota->nt_crd;
                        return redirect("notas/importar");
                }
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
    
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function FtechActiondownloadPDFFile(){
        try {
            // dd()
        
            $params = array(
                "username" => $username,
                "password" => $password,
                "prefijo" =>  $prefijo,
                "folio" => $folio,
            );

            $client = new SoapClient($wsdlUrl);
            $response = $client->__soapCall("FtechAction.downloadPDFFile", $params);
            dd($response);
           

            if ($response->code != 201) {
                return[
                    "code" => $response->code,
                    "Mensaje"=>$response->success,
                    "error"=> $response->error
                ];
            }
            $factura= Factura::select('id')->where('numero','=',$numero)->first();
            if (!empty($factura)) {
                $editar_factura= Factura::find($factura->id);
                $editar_factura->observaciones=$response->success;
                $editar_factura->save();
            }else{
                $nota= Nota::select('id')->where('numero','=',$numero)->first();
                $editar_nota= Nota::find($nota->id);
                $editar_nota->estado_envio=$response->success;
                $editar_nota->save();
            }
            // sleep(20);
            return redirect()->action("SoapController@FtechActiongetCUFEFile", [
                "prefijo" => $prefijo,
                'folio' =>$folio,
                'id' => $id,
                'numero' => $numero,
                "tokenOperacion" => $tokenOperacion,
            ]);
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------
    public function DownloadXMLFile(Request $request){
        try {
            $prefijo=$request->prefijo;
            $folio=$request->folio;

            $pricesClass = new Params();
            $prices = $pricesClass->getparams();
            $username    = $prices['username'];
            $password   = $prices['password'];
            $wsdlUrl = $prices['wsdlUrl'];


            $params = array(
                "username" => $username,
                "password" => $password,
                "prefijo" => $prefijo,
                "folio" => $folio,
            );

            $client = new SoapClient($wsdlUrl);
            $response = $client->__soapCall("FtechAction.downloadXMLFile", $params);
            
            $array = array(
                "code" => $response->code,
                "Mensaje"=>$response->success,
                "xmlbase64"=> $response->resourceData
            );
            return array($array);
            

        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
//---------------------------------------------------------------------------------------------------------------------------------------------------------
     public function FtechActiondownloadPDFFile2(Request $request){
        try {

            $prefijo=$request->prefijo;
            $folio=$request->folio;

            $pricesClass = new Params();
            $prices = $pricesClass->getparams();
            $username    = $prices['username'];
            $password   = $prices['password'];
            $wsdlUrl = $prices['wsdlUrl'];
            
            $params = array(
                "username" => $username,
                "password" => $password,
                "prefijo" => $prefijo,
                "folio" => $folio,
            );
            // dd($params);
            $client = new SoapClient($wsdlUrl);
            $response = $client->__soapCall("FtechAction.downloadPDFFile", $params);
            // dd($response);
            $array = array(
                "code" => $response->code,
                "Mensaje"=>$response->success,
                "pdfbase64"=> $response->resourceData
            );
            return array($array);
            
            
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }


}