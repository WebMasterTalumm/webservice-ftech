<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleFactura extends Model
{
    protected $table='detalles_facturas'; 

    public function facturas(){
        return $this->belongsTo('App\Factura', 'id_factura');
    }
    
}
