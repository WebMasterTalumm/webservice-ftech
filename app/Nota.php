<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $table = "notas";    

    public function detalles(){
        return $this->hasMany('App\DetalleNota', 'id_nota');
    }
    public function municipios(){
        return $this->belongsTo('App\Municipio', 'id_municipio');
    }
    public function facturas(){
        return $this->belongsTo('App\Factura', 'id_factura');
    }
}
