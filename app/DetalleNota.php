<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleNota extends Model
{
    protected $table='detalles_notas'; 

    public function facturas(){
        return $this->belongsTo('App\NotaCredito', 'id_nota');
    }
}
