<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = "municipios";    

    public function factura(){
        return $this->belongsTo('App\Factura', 'id_municipio');
    } 

    public function emisor(){
        return $this->belongsTo('App\EmisorFactura', 'id_municipio');
    }
}
