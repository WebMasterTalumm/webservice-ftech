<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmisorFactura extends Model
{
    protected $table='emisores';

    public function municipios(){
        return $this->belongsTo('App\Municipio', 'id_municipio');
    }
}
