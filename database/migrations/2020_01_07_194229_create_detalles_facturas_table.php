<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallesFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_factura');
            $table->integer('referencia');
            $table->string('descripcion', 100);
            $table->integer('mo');
            $table->integer('cantidad');
            $table->string('um', 20);
            $table->integer('precio_unit');
            $table->double('descuento');
            $table->string('valor_total', 100);
            $table->double('impuesto');
            $table->foreign('id_factura')->references('id')->on('facturas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_facturas');
    }
}
