<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero', 20);
            $table->string('fecha', 20);
            $table->string('cliente', 100);
            $table->string('contacto', 100);
            $table->string('nit_o_cc', 20);
            $table->string('direccion', 50);
            $table->string('ciudad', 20);
            $table->string('telefono', 20);
            $table->double('rte_fuente')->default(0);
            $table->double('impoconsumo')->default(0);
            $table->double('rte_iva')->default(0);
            $table->double('rte_ica')->default(0);
            $table->double('rte_cree')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
