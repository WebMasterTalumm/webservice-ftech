<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
    </head>
    <body>
        <div class="container col-md-8 col-md-offset-2">

            @foreach($facturas as $factura)
              


 @foreach($facturas as $factura)
            
                        @foreach($facturas as $factura)
                            <p>numero Factura: {{$factura->numero}}</p>

                                @foreach($factura['detalles'] as $detalle)
                                    <p>{{$detalle->referencia}} - Descripcion: {{$detalle->referencia}}</p>
                                    
                                @endforeach
                        @endforeach
                        <?php
                    $ident_tipo_doc="INVOIC";
                    $nombre_emisor="NATURAL MEDY DISTRIBUCIONES SAS ";
                    $nit_emisor="805030120-4";
                    $ciudad_emisor="CL 38N 3CN 122";
                    // separar nit cliente de codigo
                    $nit=$factura->nit;
                    $nit_cliente=explode(" ", $nit);
                    $nit_adquiriente= $nit_cliente[0];
                    //separar y convertir fecha y hora que piden
                    $fecha_hora=$factura->fecha;
                    $fecha_y_hora=explode(" ",  $fecha_hora);
                    // $fec=explode("-",  $fecha_hora[0]);
                    $fech=strtotime($fecha_y_hora[0]);
                    $fec= explode("-", $fecha_y_hora[0]);
                    $mes1=$fec[1];
                    $time_12=$fecha_y_hora[1].' PM';
                        switch ($mes1) {
                            case 'ENE':
                                $mes='01';
                                break;
                            case 'FEB':
                                $mes='02';
                                break;
                            case 'MAR':
                                $mes='03';
                                break;
                            case 'ABR':
                                $mes='04';
                                break;
                            case 'MAY':
                                $mes='05';
                                break;
                            case 'JUN':
                                $mes='06';
                                break;
                            case 'JUL':
                                $mes='07';
                                break;
                            case 'AGO':
                                $mes='08';
                                break;
                            case 'SEP':
                                $mes='09';
                                break;
                            case 'OCT':
                                $mes='10';
                                break;
                            case 'NOV':
                                $mes='11';
                                break;
                            case 'DIC':
                                $mes='12';
                                break;         
                        };
                        $hora1= new DateTime($time_12);
                    $time_24 = $hora1->format('H:i:s');
                    
                ?>
                <ENC>
                    <ENC_1>INVOIC</ENC_1><br>
                    <ENC_2>{{$nit_emisor}}</ENC_2><br> <!--Identificación del obligado a facturar electrónico - NIT.--> 
                    <ENC_3>{{$nit_adquiriente}}</ENC_3> <br> <!--Identificación del adquiriente - NIT. -->
                    <ENC_4>UBL 2.1</ENC_4> <br>
                    <ENC_5>DIAN 2.1</ENC_5><br>
                    <ENC_6>{{$factura->numero}}</ENC_6><br> <!-- Número de documento (factura o factura cambiaria, 
                    nota crédito, nota débito). Incluye prefijo + consecutivo de factura autorizados por la DIAN.-->
                    <ENC_7>{{$fec[0]}}-{{$mes}}-{{$fec[2]}}</ENC_7><br><!--"Fecha de emisión de la factura/nota. Formato AAAA-MM-DD 
                    Considerando zona horaria de Colombia (-5)"-->
                    <ENC_8>{{$time_24}}</ENC_8><br><!--23:16:23-05:00-->
                    <ENC_9>01</ENC_9><br><!--"Tipo de factura/Nota.FACTURA DE VENTA NACIONAL"   01=nacional 09=importacion-->
                    <ENC_10>COP</ENC_10><br><!--"Divisa consolidada aplicable a toda la factura/nota.No se permiten códigos diferetes a los establecidos en la tabla"-->
                    <ENC_15>4</ENC_15><br><!--"Número total de líneas en el documento.Rechazo Si el valor de  ENC _15<> número de ocurrencias del ITE_1"-->
                    <ENC_16>2019-11-14</ENC_16><br><!--Fecha de vencimiento. Formato AAAA-MM-DD -->
                    <ENC_20>2</ENC_20><br>
                    <!--"Código que describe el “ambiente de destino donde será procesada la validación previa de este documento electrónico”; 
                    este código es el testigo de que el valor registrado en cbc:UUID.@schemeID es lo que desea realizar el HFE: en igualdad 
                    confirma el ambiente y en desigualdad rechaza el procesamiento.Producción"-->
                    <ENC_21>09</ENC_21><br>
                    <!--"Indicador del tipo de operación Rechazo: Si contiene un valor distinto a los definidos en el grupo en la tabla 38"-->
                </ENC>
                <br>
                <EMI>
                        <EMI_1>1</EMI_1>
                        <!--"Tipo de identificación - Tipos de Persona Identificador de tipo de organización 
                        jurídica de la de personan Rechazo si se envíe un valor diferente a los establecidos"-->
                        <EMI_2>{{$nit_emisor}}</EMI_2><!--Identificación del obligado a facturar electrónico - NIT.--> 
                        <EMI_3>31</EMI_3>
                        <EMI_4>48</EMI_4><!--"Régimen al que pertenece el emisor Nota:En ubl 2.1 este campo es requerido por la DIAN"-->
                        <EMI_6>{{$nombre_emisor}}</EMI_6>
                        <EMI_7>{{$nombre_emisor}}</EMI_7>
                        <EMI_10>{{$ciudad_emisor}}</EMI_10>
                        <EMI_11>11</EMI_11><!--Código del Departamento.-->
                        <EMI_13>BOGOTÁ, D.C.</EMI_13>
                        <EMI_15>CO</EMI_15>
                        <!-- <EMI_18>Av. Jiménez #7 - 13</EMI_18> -->
                        <EMI_19>Bogotá</EMI_19>
                        <EMI_21>COLOMBIA</EMI_21>
                        <EMI_22>2</EMI_22>
                        <EMI_23>11001</EMI_23>
                        <EMI_24>FACTURADOR DE EJEMPLO</EMI_24>
                        <EMI_25>0910;7120</EMI_25>
                    <TAC>
                        <TAC_1>O-07;O-09;O-14;O-36;O-48</TAC_1>
                    </TAC>
                    <DFE>
                        <DFE_1>11001</DFE_1>
                        <DFE_2>11</DFE_2>
                        <DFE_3>CO</DFE_3>
                        <DFE_5>COLOMBIA</DFE_5>
                        <DFE_6>Bogotá</DFE_6>
                        <DFE_7>BOGOTÁ, D.C.</DFE_7>
                        <DFE_8>Av. Jiménez #7 - 13</DFE_8>
                    </DFE>
                    <ICC>
                        <ICC_9>SETT</ICC_9>
                    </ICC>
                        <CDE>
                        <CDE_1>1</CDE_1>
                        <CDE_2>Conny Vargas</CDE_2>
                        <CDE_3>31201200</CDE_3>
                        <CDE_4>cmvargas@mail.com.co</CDE_4>
                    </CDE>
                        <GTE>
                        <GTE_1>01</GTE_1>
                        <GTE_2>IVA</GTE_2>
                    </GTE>
                </EMI>


                
            @endforeach   
        </div>
    </body>
</html>

