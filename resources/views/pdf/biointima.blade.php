<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example 2</title>
    <link rel="stylesheet" href="assets/plantilla_1/style.css" media="all"/>
    <!-- <link rel="stylesheet" href="/assets/font/SourceSansPro-Regular.ttf"> -->
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="assets/plantilla_1/logo.png">
      </div>
      <div id="company">
        <h2 class="name">Company Name</h2>
        <div>455 Foggy Heights, AZ 85004, US</div>
        <div>(602) 519-0450</div>
        <div><a href="mailto:company@example.com">company@example.com</a></div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">INVOICE TO:</div>
          <h2 class="name">John Doe</h2>
          <div class="address">796 Silver Harbour, TX 79273, US</div>
          <div class="email"><a href="mailto:john@example.com">john@example.com</a></div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <!-- <th class="no">#</th> -->
            <th class="no">REFERENCIA</th>
            <th class="no">DESCRIPCION<br>(DESCRIPTION)</th>
            <th class="no">TALLAS</th>
            <th class="no">COLOR</th>
            <th class="no">CANTIDAD</th>
            <th class="no">VR.UNITARIO</th>
            <th class="total">VALOR TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="no">01</td>
            <td class="no">Website Design Creating a recognizable </td>
            <td class="no">$40.00</td>
            <td class="no">30</td>
            <td class="no">$1,200.00</td>
            <td class="no">30</td>
            <td class="total">$1,200.00</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="3"></td>
            <td colspan="3">SUBTOTAL</td>
            <td>$5,200.00</td>
          </tr>
          <tr>
            <td colspan="3"></td>
            <td colspan="3">TAX 25%</td>
            <td>$1,300.00</td>
          </tr>
          <tr>
            <td colspan="3"></td>
            <td colspan="3">GRAND TOTAL</td>
            <td>$6,500.00</td>
          </tr>
        </tfoot>
      </table>
      <div id="thanks">Thank you!</div>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
      </div>
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>