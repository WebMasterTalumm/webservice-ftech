<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Factura Electronica</title>
    <link rel="stylesheet" href="assets/plantilla_1/style.css" media="all"/>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> -->
    <!-- <link rel="stylesheet" href="/assets/font/SourceSansPro-Regular.ttf"> -->
  </head>
  <body>
    <header class="clearfix">
        <div id="logo">
          <img src="assets/plantilla_1/logo.png">
        </div>
        <div id="company">
            <h2 class="name">DOLORMED CENTRO INTEGRAL <br> EN MANEJO DE DOLOR S.A.S.</h2>
            <div>NIT: 900442930-6</div>
            <!-- <div>Régimen: No responsables del IVA</div> -->
            <div>Persona Jurídica</div>
            <div>CRA 33 A #24-17, Tuluá, Valle del Cauca</div>
            <div>Tel. 2257767, 3173670803</div>
            <div>dolormedsas@gmail.com</div>

      </div>
      <div id="qr">
      <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(171)->generate('https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey=yva3xgosp7gxrwaj84agqggjihm0gb2fttbbqxzy7arhda7bsnyr8e60lsnps588ndksfj5gtgw44pmbfag9105auysg4fm1')) }} ">
      </div>
      <div class="cufe title m-b-md" >
        <b>CUFE/CUDE: </b>1917497702c310c82d23d863f45837f97a5688fa5020e6b60041af2a47b6e9cd0961f0814c352f659133c97fe300b27f
      </div>
    </header>
    <main>
      <div id="info" class="clearfix">
        <div class="client">
          <b>Cliente:</b>&nbsp; <br>
          <b>NIT:</b>&nbsp; 860009578<br>
          <b> Dirección:</b>&nbsp; CL 22N 6AN 24 OF 701,CALI, Valle del Cauca, COLOMBIA <br>
          <b>Teléfono:</b>&nbsp; 2608203 &nbsp; <b>Email:</b> john@example.com <hr>
         <b>Tipo de negociación:</b>&nbsp; Contado<br>
         <b>Medio de Pago:</b>&nbsp; En efectivo</p>
        </div>
        <div class="fact">
        <b>Factura electrónica de venta:</b> &nbsp;  FEDL <b>1</b><br>
        <b>Moneda:</b>&nbsp; COP Colombia, Pesos<br>
        <b>Fecha firmado:</b>&nbsp; 25/06/2020 14:49:14<br>
        <b>Fecha de emisión:</b>&nbsp; 08/04/2020 <br>
        <b>Fecha de vencimiento:</b>&nbsp; 08/042020 <br>
      </div>
      </div>
      <div id="details">
          <div class="cabe">
            <div class="ref">Referencia</div>
            <div class="descr">Descripcion</div>
            <div class="mo">U.Medida</div>
            <div class="cant">Cantidad</div>
            <div class="precuni">Precio.U</div>
            <div class="iva">Iva</div>
            <div class="dcto">Dcto.</div>
            <div class="tot">Total</div>
          </div>
          <div class="cont">
            <div class="contref">39305 <br>39141 <br> 37206 <br>39202 <br>21101</div>
            <div class="contdescr">CONSULTA PRIORITARIA POR MEDICINA GENERAL<br>INMOVILIZACION EN MIEMBRO SUPERIOR/ INFERIOR TOTAL O PARCIAL
            <br>SALA DE PROCEDIMIENTOS DE CURACIONES <br>RADIOGRAFIA DE MUNECA
            <br>RADIOGRAFIA DE RODILLA AP,LATERAL</div>
            <div class="contmo"> <br>  <br>  <br> <br></div>
            <div class="contcant">1.00 <br>1.00 <br>1.00 <br>1.00 <br>1.00</div>
            <div class="contprecuni">67,600.00 <br>35,100.00 <br>55,000.00 <br>20,800.00 <br>49,400.00</div>
            <div class="contiva">0.00 <br>0.00 <br>0.00 <br> 0.00 <br>0.00</div>
            <div class="contdcto">0.00 <br>0.00 <br>0.00 <br>0.00 <br>0.00 </div>
            <div class="conttot">67,600.00 <br>35,100.00 <br>55,000.00 <br>20,800.00 <br>49,400.00 <br></div>
          </div>
        <div id="wrapper" class="clearfix">
          <div id="first">&nbsp;<b>Observaciones:</b>
          &nbsp;Esta factura es un título valor de acuerdo al art. 774 del C.C. y una vez aceptada declara haber recibido los bienes y servicios a satisfacción

          </div>

          <div id="second">
            <div class="subto">&nbsp;Subtotal: </div>
            <div class="totsubto">&nbsp;$ 227,900.00</div>
            <div class="cargo">&nbsp;Cargos: </div>
            <div class="totcargo">&nbsp;$ 0.0</div>
            <div class="descu">&nbsp;Descuentos: </div>
            <div class="totdesc">&nbsp;$ 0.00</div>
            <div class="tiva">&nbsp;Iva: </div>
            <div class="totiva">&nbsp;$ 0.00</div>
            <div class="totop">&nbsp;Total de la operación: </div>
            <div class="tottotop">&nbsp;$ 227,900.00</div>
            <!--<div class="retiv">&nbsp;ReteIVA:  </div>
            <div class="tottretiv">&nbsp;$ </div>
            <div class="retic">&nbsp;ReteICA:  </div>
            <div class="tottretic">&nbsp;$ </div>
            <div class="retfu">&nbsp;ReteFuente:  </div>
            <div class="totretfu">&nbsp;$ </div>
            <div class="netfac">&nbsp;Neto Factura:  </div>
            <div class="totnetfac">&nbsp;$ 4.427.177,66</div> -->
          </div>
        </div><br>
        <div id="informacion" class="clearfix">
          <div id="observacion">
          <table>
              <thead>
                <tr>
                  <th>Impuesto</th>
                  <th>Base </th>
                  <th>Tarifa</th>
                  <th>Importe</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th Colspan="4" align="center">Impuestos</th>
                </tr>
                <tr>
                  <td>01 IVA</td>
                  <td>$0.00</td>
                  <td>0,00%</td>
                  <td>$0.00</td>
                </tr>
                <br><br>
               <!-- <tr>
                  <th Colspan="4" align="center">Retenciones</th>
                </tr>
                <tr>
                  <td>05 ReteIVA</td>
                  <td>2.999.400,00</td>
                  <td>15,00%</td>
                  <td>569.886,00</td>
                </tr>
                <tr>
                  <td>06 ReteICA</td>
                  <td>2.999.400,00</td>
                  <td>0,77%</td>
                  <td>569.886,00</td>
                </tr>
                <tr>
                  <td>06 ReteFuente </td>
                  <td>2.999.400,00</td>
                  <td>2,5%</td>
                  <td>569.886,00</td>
                </tr> -->
              </tbody>
            </table>
          </div>
          <div id="doc_rela">
          <!-- <table>
              <thead>
                <tr>
                  <th Colspan="3">DOCUMENTOS RELACIONADOS</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>No.:</td>
                  <td>Tipo</td>
                  <td>Fecha:</td>
                </tr>
                <tr>
                  <td>TCFA15727</td>
                  <td>Factura</td>
                  <td>28/05/2020</td>
                </tr>
                <tr>
                  <th Colspan="3">CUFE/CUDE</td>
                </tr>
                <tr>
                  <td Colspan="3">1917497702c310c82d23d863f45837f97a5688fa5020 <br> e6b60041af2a47b6e9cd0961f0814c352f659133c97fe300b27f</td>
                </tr>
              </tbody>
            </table> -->
          </div>
        </div>
        <div class="otroinfo">
       <div><b>Validacion previa:</b>02 Documento validado por la Dian &nbsp;&nbsp;<b>Fecha :</b> 2020-03-24 &nbsp;<b>Hora:</b> 14:42:05-05:00</div>
       <div><b>Operador Tecnológico: </b>CADENA SAS  NIT: 890390534-0</div>
       <div align="left"><b>Representación grafica generada por </b> <a href="https://pagopass.co/">Pagopass.co</b></a></div><br><br>
       <div align="center" style="font-size:11px;">AUTORIZACIÓN FACTURA ELECTRÓNICA DE VENTA No. 18764000764900 VÁLIDA DESDE 2020-07-10 HASTA
       2021-07-10 RANGO DESDE <br> FEDL1 HASTA FEDL25000. VIGENCIA 24 MESES</div>
        </div>
    </main>

    <footer><br>
    Esta factura es un título valor de acuerdo al art. 774 del C.C. y una vez aceptada declara haber recibido los bienes y servicios a satisfacción
    </footer>
  </body>
</html>
