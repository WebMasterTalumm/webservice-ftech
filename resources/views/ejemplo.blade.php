<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Utilizando ajax con Axios</title>
<meta name="description" content="Utilizando ajax con Axios">
<meta name="author" content="Blastcoding">
<link rel="stylesheet">
</head>
<body>
<input type="button" name="" id="btnajax" class="btn btn-primary" role="button" onclick="axaj()" value="click me">
@include("tochange")
<script src={{ asset('js/app.js')}}></script>
<script>
function axaj(){
    axios.get('/cliente/message')
    .then(function (response) {
        // handle success
        console.log(response);
    })
    .catch(function (error) {
        // handle error
        console.log(error);
    });
}
</script>
</body>
</html>
