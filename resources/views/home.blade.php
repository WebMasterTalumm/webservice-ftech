@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Panel</div>

                <div class="card-body" align="center">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="subir/facturas"><button class="myButton"><i class="fa fa-file-text"></i> &nbsp; <b>Subir Facturas</b></button></a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="subir/notas"><button class="mButton"><i class="fa fa-file-text-o"></i> &nbsp; <b>Subir Notas</b></button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
                <style>


.myButton {
	box-shadow: 3px 4px 0px 0px #1564ad;
	background:linear-gradient(to bottom, #79bbff 5%, #378de5 100%);
	background-color:#79bbff;
	border-radius:5px;
	border:1px solid #337bc4;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:17px;
	font-weight:bold;
	padding:12px 44px;
	text-decoration:none;
	text-shadow:0px 1px 0px #528ecc;
}
.myButton:hover {
	background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
	background-color:#378de5;
}
.myButton:active {
	position:relative;
	top:1px;
}
.mButton {
	box-shadow: 3px 4px 0px 0px #008000;
	background:linear-gradient(to bottom, #00BB2D 5%, #77DD77 100%);
	background-color:#008000;
	border-radius:5px;
	border:1px solid #337bc4;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:17px;
	font-weight:bold;
	padding:12px 44px;
	text-decoration:none;
	text-shadow:0px 1px 0px #528ecc;
}
.mButton:hover {
	background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
	background-color:#378de5;
}
.mButton:active {
	position:relative;
	top:1px;
}
                </style>
