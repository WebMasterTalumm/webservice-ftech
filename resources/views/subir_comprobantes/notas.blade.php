<!DOCTYPE html>
<html>
  <head>
    <title></title>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="/dropzone/dist/dropzone.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- JS -->
    <script src="/dropzone/dist/dropzone.js" type="text/javascript"></script>
        <style>
            .button {
            display: inline-block;
            padding: 15px 25px;
            font-size: 15px;
            cursor: pointer;
            text-align: center;
            text-decoration: none;
            outline: none;
            color: #fff;
            background-color: #008CBA;
            border: none;
            border-radius: 15px;
            box-shadow: 0 9px #999;
            margin-top: 10px;
            margin-bottom: 30px;
            }

            .button:hover {background-color: #008CBA}

            .button:active {
            background-color: #008CBA;
            box-shadow: 0 5px #666;
            transform: translateY(4px);
            }
        </style>
  </head>
  <body>

      <!-- Dropzone -->
        <div class="col-lg-12">
        <CENTER><h1>NOTAS</h1></CENTER>
            <div class="form-group">
                <center>
                    <div class="col-sm-12" >
                        <form action="{{route('notas.fileupload')}}" class='dropzone' >
                            {{csrf_field()}}
                        </form>
                    </div>
                </center>
            </div>
            <div class="form-group">
                    <center>
                        <div class="col-lg-12">
                            <a href="/notas/importar"><button class="button">Firmar Comprobante</button></a>
                        </div><br>
                    </center>
                </div>
            </div><br><br>
            @if(session('status'))
            <div class="form-group">
                <div class="col-lg-12"><br><br>
                    <div class="alert alert-danger">
                    <strong>{{session('status')}}</strong>
                    </div>
                </div>
            </div>
            @endif
            @if(session('msg'))
            <div class="form-group"><br><br>
            @php
                $ult_nume=session()->get('msg')[0]->numero;
                $fol=substr($ult_nume,'0','2');
                $num=substr($ult_nume,'2')+1;

            @endphp
            <center><h3>Ultimos Comprobantes Firmados se espera {{$fol.$num}}</h3></center>
                <div class="col-lg-12"><br><br><br>
                        @foreach(session()->get('msg')  as $in)
                        <div class="alert alert-success">
                            <strong>Factura: </strong> <?=$in->numero?> ; <strong>Cliente: </strong> <?=$in->razon_social?> ; <strong>Cufe:</strong> <?=$in->cufe?> <strong>Estado envio: </strong> <?=$in->estado_envio?>
                        </div>
                        @endforeach
                </div>
            </div>
            @endif

        </div>
      <script>
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone(".dropzone",{
                maxFilesize: 3,  // 3 mb
                acceptedFiles: ".jpeg,.jpg,.png,.pdf",
            });
            myDropzone.on("sending", function(file, xhr, formData) {
                var name= file.upload.filename;
                console.log(name);
                // document.write(name);
            formData.append("_token", CSRF_TOKEN);
            });
        </script>
      <!-- -->
    </div>
  </body>
</html>
